//
//  STChatUser.m
//  STChatKit
//
//  Created by EDY on 2023/7/12.
//

#import "STChatUser.h"
#import <BGFMDB/BGFMDB.h>
#import "STChatDefine.h"


@implementation STChatUser

+ (instancetype)userWithUid:(NSString *)uid name:(NSString *)name avatar:(NSString *)avatar {
    
    STChatUser * tempUser = [STChatUser findUserById:uid];
    if (!tempUser) {
        tempUser = [STChatUser new];
    }
    
    if (uid && uid.length) {
        tempUser.UserId = uid;
    }
    
    if (name && name.length) {
        tempUser.Nickname = name;
    }
    if (avatar && avatar.length) {
        tempUser.HeadImage = avatar;
    }
    
    return tempUser;
}

+ (void)saveUserWithUid:(NSString *)uid name:(NSString *)name avatar:(NSString *)avatar {
    STChatUser * user = [STChatUser userWithUid:uid name:name avatar:avatar];
    [user saveToTable];
}

+ (STChatUser *)findUserById:(NSString *)uid {
    NSString * where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"UserId"),bg_sqlValue(uid)];
    STChatUser * user = [STChatUser bg_find:nil where:where].firstObject;
    if (!user) {
        
    }
    return user;
}


+ (NSArray *)bg_uniqueKeys {
    return @[@"UserId"];
}

+ (NSArray *)bg_ignoreKeys {
    return @[@"tempModel"];
}


- (void)saveToTable {
    [self bg_saveOrUpdateAsync:^(BOOL isSuccess) {
        NSLog(@"用户信息保存 %@",isSuccess ? @"成功": @"失败");
    }];
}


@end
