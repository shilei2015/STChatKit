//
//  STChatMessage.m
//  STChatKit
//
//  Created by EDY on 2023/7/12.
//

#import "STChatMessage.h"
#import <BGFMDB/BGFMDB.h>

#import "STChatManager.h"

#import "STChatUser.h"
#import "STChatSession.h"
#import "STChatImage.h"
#import <STBaseLib/STBaseLib.h>


@interface STChatMessage ()

@property (nonatomic , copy) NSString * sslocalExtJson;

@end


@implementation STChatMessage

+ (NSArray *)bg_ignoreKeys {
    return @[@"updateHandle",@"noSaveType"];
}
+ (NSArray *)bg_uniqueKeys {
    return @[@"messageId"];
}


+ (NSString *)tableNameNameChannel:(NSString *)channelId {
    return [NSString stringWithFormat:@"%@_%@",@"ChatMessageTable",channelId];
}

+ (NSString *)channelWithUid:(NSString *)peer {
    if ([STChatManager.chatUid integerValue] < [peer integerValue]) {
        return [NSString stringWithFormat:@"%@_%@",STChatManager.chatUid,peer];
    }
    return [NSString stringWithFormat:@"%@_%@",peer,STChatManager.chatUid];
}

+ (NSString *)tableNameNameWithPeer:(NSString *)peer {
    NSString * channelN = [self channelWithUid:peer];
    NSString * tableN = [self tableNameNameChannel:channelN];
    return tableN;
}


+ (NSArray *)messagesWithChannel:(NSString *)channel {
    return [STChatMessage bg_findAll:[STChatMessage tableNameNameChannel:channel]];
}


+ (instancetype)messageWithText:(NSString *)text {
    STChatMessage * message = [STChatMessage new];
    message.msgType = STChatMessageTypeText;
    message.textMessage = text;
    
    return message;
}

+ (instancetype)messageWithImage:(UIImage *)image {
    
    STChatImage * imageObj = [STChatImage new];
    imageObj.localImage = image;
    
    STChatMessage * message = [STChatMessage new];
    message.msgType =     STChatMessageTypeImage;
    message.imageObj = imageObj;
    message.createTimeSpan = [[NSDate date] timeIntervalSince1970];
    message.updateTimeSpan = [[NSDate date] timeIntervalSince1970];

    return message;
}

+ (instancetype)messageWithGiftUrlString:(NSString *)urlString relationId:(NSString *)RelationId {
    
    STChatImage * imageObj = [STChatImage new];
    imageObj.urlString = urlString;
    imageObj.RelationId = RelationId;
    
    STChatMessage * message = [STChatMessage new];
    message.msgType =     STChatMessageTypeGift;
    message.imageObj = imageObj;
    
    return message;
}



+ (NSArray *)getMessageWithCount:(NSInteger)limit channelId:(NSString *)channelId firstMessage:(nullable STChatMessage *)firstMessage complect:(nullable void (^)( NSArray<STChatMessage *>* _Nullable ))complect {
    
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";
    NSString * nowTime = [formatter stringFromDate:[NSDate new]];
    
    
    NSMutableString * where = [NSMutableString new];
    [where appendString:[NSString stringWithFormat:@"where %@<%@",bg_sqlKey(bg_createTimeKey),bg_sqlValue(firstMessage.bg_createTime?firstMessage.bg_createTime:nowTime)]];
    [where appendFormat:@"order by %@ ",bg_sqlKey(bg_createTimeKey)];
    [where appendFormat:@"desc"];//降序
//        [where appendFormat:@"asc"];//升序
    [where appendFormat:@" limit %@",@(limit)];
    
    NSString * tableName = [STChatMessage tableNameNameChannel:channelId];
    
    NSArray * arr = [STChatMessage bg_find:tableName where:where];
    
    NSLog(@"【%@】获取数据",tableName);
    
    for (STChatMessage * msg in arr) {
        NSLog(@"%@ %@",msg.bg_createTime,msg.textMessage);
    }
    
    NSMutableArray *newlist = [[NSMutableArray alloc] init];
    for (id obj  in [arr reverseObjectEnumerator]) {
        [newlist addObject:obj];
    }
    
    return newlist;
}

+ (void)deleteRecordWithChannelId:(NSString *)channelId {
    
    NSString * tName = [STChatMessage tableNameNameChannel:channelId];
    [STChatMessage bg_clearAsync:tName complete:^(BOOL isSuccess) {
        NSLog(@"【%@】清空-%@",tName,isSuccess ? @"成功" : @"失败");
    }];
}


#pragma mark 对象方法
- (NSString *)bg_tableName {
    NSString * GTBName = [STChatMessage groupTableNameWithGroup:self.group];
    if (GTBName) {return GTBName;}
    return [STChatMessage tableNameNameChannel:self.channelId];
}

+ (NSString *)groupTableNameWithGroup:(NSString *)groupId {
    if ([groupId isKindOfClass:[NSString class]]) {
        if (groupId.length) {
            return [NSString stringWithFormat:@"Group_%@_%@",groupId,[STChatManager chatUid]];
        }
    }
    return nil;
}

- (BOOL)isMineSend {
    return [self.fromUid isEqualToString:[STChatManager chatUid]];
}

- (void)saveMsgToTable {
    
    if (self.sychonizeIgnore) {return;}
    
    BOOL isExist = YES;
    if (!self.messageId) {
        isExist = NO;
        NSString * messageId = [NSString stringWithFormat:@"%zd",(long)([[NSDate new] timeIntervalSince1970] * 1000)];
        self.messageId = messageId;
        NSLog(@"新建messageId = %@",self.messageId);
        
        STChatMessage * lastOne = [STChatMessage bg_lastObject:[self bg_tableName]];
        NSInteger difTime = ABS(self.createTimeSpan - lastOne.createTimeSpan);
        
        if (difTime > 5*60) {
            self.isAfter5Min = YES;
        }
    }
    
    [self bg_saveOrUpdateAsync:^(BOOL isSuccess) {
        NSLog(@"【%@】%@消息-保存 %@",self.bg_tableName,(isExist ? @"更新" : @"插入"),isSuccess ? @"成功" : @"失败");
    }];
}


- (NSString *)chatTargetUid {
    if ([self.fromUid isEqualToString:STChatManager.chatUid]) {
        return self.toUid;
    }
    return self.fromUid;
}


- (STChatUser *)chatTargetUser {
    STChatUser * tempUser = [STChatUser findUserById:[self chatTargetUid]];
    return tempUser;
}

- (STChatUser *)fromUser {
    STChatUser * tempUser = [STChatUser findUserById:self.fromUid];
    return tempUser;
}

- (STChatUser *)toUser {
    STChatUser * tempUser = [STChatUser findUserById:self.toUid];
    return tempUser;
}

- (NSString *)contentText {
    return [STChatManager messageContent:self];
}

- (ChatSendComplectType)complectType {
    
    if (_complectType == ChatSendComplectTypeSending) {
        if ([[NSDate new] timeIntervalSince1970] - self.createTimeSpan > 1 * 30) {
            _complectType = ChatSendComplectTypeFail;
        }
    }
    
    return _complectType;
}


@end
