//
//  UIScrollView+SLRefresh.h
//  STBasic_Objc
//
//  Created by 244235126@qq.com on 2021/8/4.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "STRefreshConst.h"
#import "STRefreshProtocal.h"


NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (SLRefresh)

+ (void)configDefaultEmptyTitle:(NSString *)title detail:(NSString *)detail imgName:(NSString *)imgName;

/// 配置滚动视图-上拉加载、下拉刷新
/// @param api 请求地址
/// @param params 请求参数
/// @param objs 处理后返回的数据  [<SectionModel>]
/// @param requestType 网络请求的类型
/// @param pageable 是否支持分页
- (void)setRefresherWithApi:(nullable List_Api)api params:(nullable List_Parms)params objs:(nullable List_Objs)objs requestType:(RefreshNetWorkType)requestType pageable:(BOOL)pageable;

- (void)setRefresherWithApi:(nullable List_Api)api params:(nullable List_Parms)params objs:(nullable List_Objs)objs requestType:(RefreshNetWorkType)requestType pageable:(BOOL)pageable refresherConfig:(nullable void(^)(ST_Refresher *rtf))config;

///手动调用下拉刷新
- (void)startHeadRefreshWithAnimation:(BOOL)animated;

///手动调用上拉加载更多
- (void)startFootRefreshWithAnimation:(BOOL)animated;

///单section模式 - 初始化数据
- (void)emptySingle;


///重载列表数据 - 不进行请求
- (void)reloadList;


//标记查看下面使用方法
- (void)examplePositionTag;

@end

NS_ASSUME_NONNULL_END



