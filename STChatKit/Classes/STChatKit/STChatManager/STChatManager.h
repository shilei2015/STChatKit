
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <STBaseLib/STBaseLib.h>
#import "STChatMessage.h"
#import "STChatImage.h"
#import "STChatSession.h"
#import "STChatUser.h"
#import "STChatDefine.h"


typedef Class _Nullable (^ItemMsgHandle)(id _Nullable msg);
typedef void(^STChatKitGetNewMessageHandle)(STChatMessage * _Nonnull message,STChatSession * _Nonnull session);
typedef void(^STChatKitGetStategyHandle)(NSDictionary * _Nonnull data);
typedef void(^STChatKitComplectHandle)(BOOL suc,BOOL refreshToken);
typedef void(^STChatKitSendNetWorkComplectHandle)(BOOL suc);
typedef void(^STChatKitSendMessageComplectHandle)(STChatMessage * _Nullable message,id _Nullable data);



@protocol STChatKitProrocol <NSObject,SDKNetworkingProtocal>

@required
///当前应用用户id
+ (NSString *_Nullable)appUserId;

///处理发送消息
+ (void)sendMsg:(STChatMessage *_Nonnull)message complect:(nullable STChatKitSendNetWorkComplectHandle)complect loadingHud:(BOOL)showHud errorTip:(BOOL)tipError;

///处理不同类型消息展示
+ (NSString *_Nullable)messageContentText:(STChatMessage *_Nonnull)message;



@optional
///把网络请求和代理分开 <暂时未使用>
+ (Class <SDKNetworkingProtocal>_Nonnull)requestClass;


@end








NS_ASSUME_NONNULL_BEGIN

@interface STChatManager : NSObject

+ (NSString *)chatUid;

///发送消息
+ (void)sendMessage:(STChatMessage *)message toUser:(NSString *)toUserId complect:(nullable STChatKitSendMessageComplectHandle)complect;

@end







@interface STChatManager (NetWork)

///配置网络请求类
+ (void)configChatKitNetworkClass:(Class)mClass;
+ (Class<STChatKitProrocol>)networkClass;

@end








@interface STChatManager (Helper)


///通过STChatMessage获取对应的消息内容
///若消息类型为STChatMessageTypeImage return [IMG] ...
+ (NSString *)messageContent:(STChatMessage *)message;


///通过聊天对象id获取频道 channelID
+ (NSString *)channelWithUid:(NSString *)peer;

///新建Identifer
+ (NSString *)newIdentifer;

///聊天列表添加新消息
+ (void)tableView:(UITableView *)tableView addMessage:(STChatMessage *)message;

+ (void)configChatTableView:(UITableView *)tableView pageSize:(NSInteger)pageSize firstLoad:(BOOL)firstLoad targetUid:(NSString *)targetUid itemCellClass:(ItemMsgHandle)itemClassHandle;


+ (void)receiverHasReceiver:(NSString *)chatUid handle:(nullable void(^)(BOOL success))handle;

@end







@interface STChatManager (Debug)

+ (NSString *)debug_text;

+ (void)debug_sendMsgsCount:(int)msgCount toUid:(NSString *)uid showInTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
