

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STRangeModel : NSObject

@property (nonatomic , assign , readonly) NSRange matchRange;
@property (nonatomic , copy , readonly) NSString * matchText;

@property (nonatomic , assign , readonly) NSRange resultRange;
@property (nonatomic , copy , readonly) NSString * resultText;

@property (nonatomic , assign , readonly) NSInteger index;


@property (nonatomic , strong) UIColor * rangeColor;
@property (nonatomic , strong) UIFont * rangeFont;
@property (nonatomic , assign) NSUnderlineStyle underLineStyle;
@property (nonatomic , assign) NSUnderlineStyle strikethroughStyle;


@end



@interface UIView (STLocalized)

@end


@interface UILabel (STLocalize)

@property(nonatomic, assign) IBInspectable BOOL isTextLocalize;

- (void)config:(nullable void(^)(STRangeModel * rangeM))rangeConfig action:(nullable void(^)(STRangeModel * rangeM))tapAction;

@end


@interface UIButton (STLocalize)

@property(nonatomic, assign) IBInspectable BOOL isTextLocalize;

@end


@interface NSString (STLocalize)

///查看当前出现过的本地话配置keyvalue 【en】
+ (void)showLocalizedString;

///本地化字符串
- (NSString *)st_localized;

///本地化文本
///@"send a {file}"
///[@"send a {file}" st_documentLocalizedReplaceTagsValues:@[@"file"]];
- (NSString *)st_documentLocalizedReplaceTagsValues:(NSArray <NSString *>*)values;

///本地化文本
///"Are you sure you want to block {nickname} and {nickname2}?"  ---> "Are you sure you want to block 张三 and 李四?"
///[@"Are you sure you want to block {nickname} and {nickname2}?" st_documentLocalizedReplaceTags:@[@"{nickname}",@"{nickname2}"] withValues:@[@"张三",@"李四"]];
- (NSString *)st_documentLocalizedReplaceTags:(NSArray <NSString *>*)tags withValues:(NSArray <NSString *>*)values;


@end




NS_ASSUME_NONNULL_END
