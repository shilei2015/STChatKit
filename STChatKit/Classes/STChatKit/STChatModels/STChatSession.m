//
//  STChatSession.m
//  STChatKit
//
//  Created by EDY on 2023/7/12.
//

#import "STChatSession.h"
#import <BGFMDB/BGFMDB.h>

#import "STChatDefine.h"

#import "STChatManager.h"
#import "STChatMessage.h"
#import "STChatUser.h"

@implementation STChatSession

+ (NSArray *)bg_uniqueKeys {
    return @[@"channelId"];
}

+ (NSArray *)bg_ignoreKeys {
    return @[@"tempModel"];
}

- (NSString *)bg_tableName {
    return [STChatSession tableName];
}

+ (NSString *)tableName {
    NSString * tableName = [NSString stringWithFormat:@"%@_%@",@"ChatSessionTable",STChatManager.chatUid];
    return tableName;
}

+ (NSArray<STChatSession *> *)allSessions {
    return [STChatSession bg_find:[STChatSession tableName] limit:9999 orderBy:bg_updateTimeKey desc:YES];
}

+ (NSInteger)allSessionUnreadCount {
    __block NSInteger count = 0;
    NSString * where = [NSString stringWithFormat:@"where %@>%@",bg_sqlKey(kUnReadCountKey),bg_sqlValue(@(0))];
    NSArray <STChatSession *>* arr = [STChatSession bg_find:[STChatSession tableName] where:where];
    [arr enumerateObjectsUsingBlock:^(STChatSession * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        count += obj.unReadCount;
    }];
    
    return count;
}



+ (STChatSession *)sessionWithLastMsg:(STChatMessage *)message {
    STChatSession * session = [STChatSession sessionWithChannelId:message.channelId];
    session.channelId = message.channelId;
    session.lastMessage = message;
    return session;
}

+ (STChatSession *)sessionWithPeer:(NSString *)peer {
    NSString * channel = [STChatManager channelWithUid:peer];
    STChatSession * session = [STChatSession sessionWithChannelId:channel];
    if (!session.lastMessage) {
        session.lastMessage = [STChatMessage new];
        session.lastMessage.fromUid = peer;
    }
    return session;
}

+ (STChatSession *)sessionWithChannelId:(NSString *)channelId {

    NSString * where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"channelId"),bg_sqlValue(channelId)];
    STChatSession * session = [STChatSession bg_find:[STChatSession tableName] where:where].lastObject;
    if (!session) {
        session = [STChatSession new];
    }
    session.channelId = channelId;
    return session;
}

- (void)markAllRead {
    if (self.unReadCount) {
        self.unReadCount = 0;
        [self saveToTable];
    }
}

- (void)saveToTable {
    if (self.lastMessage.sychonizeIgnore) {return;}
    
    [self bg_saveOrUpdateAsync:^(BOOL isSuccess) {
        NSLog(@"【%@】会话更新 %@",self.bg_tableName,isSuccess ? @"成功" : @"失败");
    }];
}

+ (void)deleteAllSession {
    [STChatSession bg_clear:[STChatSession tableName]];
}

- (void)removeSession {
    NSString * where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"channelId"),bg_sqlValue(self.channelId)];
    [STChatSession bg_delete:[STChatSession tableName] where:where];
}


@end
