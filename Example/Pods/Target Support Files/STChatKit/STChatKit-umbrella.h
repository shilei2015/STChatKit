#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "STChatKit.h"
#import "STChatManager.h"
#import "STChatDefine.h"
#import "STChatImage.h"
#import "STChatMessage.h"
#import "STChatSession.h"
#import "STChatUser.h"

FOUNDATION_EXPORT double STChatKitVersionNumber;
FOUNDATION_EXPORT const unsigned char STChatKitVersionString[];

