//
//  STTextField.m
//  STBaseLib
//
//  Created by EDY on 2023/6/14.
//

#import "STTextField.h"
#import "NSString+STBase.h"

@interface STTextField ()

@property (nonatomic , assign , readwrite) BOOL regularOK;

@end


@implementation STTextField


- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self customSetting];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self customSetting];
    }
    return self;
}

- (void)customSetting {
    self.delegate = self;
}



#pragma mark <UITextFieldDelegate>
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    NSInteger existedLength = textField.text.length;
    NSInteger selectedLength = range.length;
    NSInteger replaceLength = string.length;
    NSInteger ignoreLength = 0;
    UITextRange *selectedRange = [textField markedTextRange];
    NSString *newText = [textField textInRange:selectedRange]; //获取高亮部分
    ignoreLength = newText.length;
    
    NSInteger length = 0;
    
    if (selectedRange) {
        length = existedLength - ignoreLength + replaceLength;
    } else {
        length = existedLength - selectedLength + replaceLength;
    }
    
    NSString * str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"%@",str);
    if (self.maxLength > 0 && length > self.maxLength) {
        return NO;
    }
    
    
    if (self.regularTypeIn && string.length) {
        return [string rugularWithPattern:@"^[0-9]+$"];
    }
    
    
    return YES;
}


- (BOOL)regularOK {
    _regularOK = YES;
    if (self.regularFormat) {
        _regularOK = [self.text rugularWithPattern:self.regularFormat];
    }
    return _regularOK;
}


- (void)textFieldDidChangeSelection:(UITextField *)textField {
    if (self.textChanged) {
        self.textChanged(self);
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.nextAction) {
        self.nextAction(self);
    }
    return YES;
}

@end
