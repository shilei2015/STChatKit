//
//  STChatImage.h
//  STChatKit
//
//  Created by EDY on 2023/7/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STChatImage : NSObject

///原图地址
@property(nonatomic, copy) NSString * _Nullable urlString;

///本地图片
@property (nonatomic,strong) UIImage * _Nullable localImage;

///文件关联 id
@property(nonatomic, copy) NSString * _Nullable RelationId;


@end

NS_ASSUME_NONNULL_END
