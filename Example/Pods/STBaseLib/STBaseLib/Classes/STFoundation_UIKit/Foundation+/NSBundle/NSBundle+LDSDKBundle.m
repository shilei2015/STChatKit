//
//  NSBundle+LDSDKBundle.m
//  SDKTestProject
//
//  Created by 石磊 on 2023/1/5.
//

#import "NSBundle+LDSDKBundle.h"

#define kIsBundleDevelop NO


@implementation NSBundle (LDSDKBundle)


#pragma mark - 获取 BRPickerView.bundle
+ (instancetype)st_SDKBundle:(nullable NSString *)bundleName {
    static NSBundle *pickerBundle = nil;
    if (pickerBundle == nil) {
        /*
            先拿到最外面的 bundle。
            对 framework 链接方式来说就是 framework 的 bundle 根目录，
            对静态库链接方式来说就是 target client 的 main bundle，
            然后再去找下面名为 BRPickerView 的 bundle 对象。
         */
        
        if (bundleName) {
            NSString * path = [[NSBundle mainBundle] pathForResource:bundleName ofType:@"bundle"];
            pickerBundle = [NSBundle bundleWithPath:path];
        } else {
            pickerBundle = [NSBundle mainBundle];
        }
    }
    return pickerBundle;
}


#pragma mark - 获取国际化后的文本

+ (NSString *)ld_localizedStringForKey:(NSString *)key {
    return [self ld_localizedStringForKey:key value:nil language:nil];
}

+ (NSString *)ld_localizedStringForKey:(NSString *)key language:(NSString *)language {
    return [self ld_localizedStringForKey:key value:nil language:language];
}

+ (NSString *)ld_localizedStringForKey:(NSString *)key value:(NSString *)value language:(NSString *)language {
    static NSBundle *bundle = nil;
    if (bundle == nil) {
        // 如果没有手动设置语言，将随系统的语言自动改变
        if (!language) {
            // 系统首选语言
            language = [NSLocale preferredLanguages].firstObject;
        }
        
        if ([language hasPrefix:@"en"]) {
            language = @"en";
        } else if ([language hasPrefix:@"zh"]) {
            if ([language rangeOfString:@"Hans"].location != NSNotFound) {
                language = @"zh-Hans"; // 简体中文
            } else { // zh-Hant、zh-HK、zh-TW
                language = @"zh-Hant"; // 繁體中文
            }
        } else {
            language = @"en";
        }
        
        // 从 bundle 中查找资源
        bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]];
    }
    value = [bundle localizedStringForKey:key value:value table:nil];
    
    return [[NSBundle mainBundle] localizedStringForKey:key value:value table:nil];
}



@end


