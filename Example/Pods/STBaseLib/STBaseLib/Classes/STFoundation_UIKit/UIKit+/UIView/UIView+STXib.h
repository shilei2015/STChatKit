//
//  UIView+STXib.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import <UIKit/UIKit.h>



NS_ASSUME_NONNULL_BEGIN


@interface UIResponder (STBundle)

+ (NSBundle *)selfBundle;
- (NSBundle *)selfBundle;

@end


@interface UIView (STXib)
+ (instancetype)xibView;
+ (instancetype)xibViewWithRadius:(float)radius;

+ (UINib *)getNib;
+ (NSString *)className;
- (NSString *)className;

@end




NS_ASSUME_NONNULL_END
