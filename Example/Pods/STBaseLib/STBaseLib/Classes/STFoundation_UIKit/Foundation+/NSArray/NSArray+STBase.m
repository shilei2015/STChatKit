//
//  NSArray+STBase.m
//  LDSDK
//
//  Created by 石磊 on 2023/2/9.
//

#import "NSArray+STBase.h"

@implementation NSArray (STBase)


@end



@implementation NSMutableArray (STBase)

- (void)sl_addObject:(id)anObject {
    if (anObject) {
        [self addObject:anObject];
    }
}

- (void)sl_addSkipNolengString:(NSString *)string {
    if ([string isKindOfClass:[NSString class]] && string.length) {
        [self addObject:string];
    }
}


///多参数示例
- (void)exchangeTitles:(NSString *)titles,... {
    
    
    va_list params;//定义一个指向个数可变的参数列表指针
    
    va_start(params, titles);//va_start 得到第一个可变参数地址
    NSString *arg;
    if (titles) {
        //将第一个参数添加到array
        id prev = titles;
        NSLog(@"%@",prev);
        
        //va_arg 指向下一个参数地址
        while ((arg = va_arg(params, NSString *))) {
            NSLog(@"%@",arg);
        }
        //置空
        va_end(params);
    }
}


@end


