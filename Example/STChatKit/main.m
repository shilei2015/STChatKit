//
//  main.m
//  STChatKit
//
//  Created by shilei2015 on 07/12/2023.
//  Copyright (c) 2023 shilei2015. All rights reserved.
//

@import UIKit;
#import "STCHATAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STCHATAppDelegate class]));
    }
}
