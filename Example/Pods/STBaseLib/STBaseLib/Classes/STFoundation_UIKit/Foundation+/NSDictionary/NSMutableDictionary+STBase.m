//
//  NSMutableDictionary+STBase.m
//  STTempPods
//
//  Created by 石磊 on 2022/3/21.
//

#import "NSMutableDictionary+STBase.h"

@implementation NSMutableDictionary (STBase)

- (void)sl_setObject:(nullable id)anObject forKey:(NSString *)aKey
{
    if (!aKey) {return;}
    if (!anObject) {
        [self setObject:@"" forKey:aKey];
    } else {
        [self setObject:anObject forKey:aKey];
    }
}


+ (NSDictionary *)dictWithJsonString:(NSString *)jsonStr {
    return [NSDictionary new];
}

- (NSString *)dictToJson {
    return @"";
}


@end


@implementation NSDictionary (STBase)

- (NSString *)st_jsonStringEncode {
    NSString *result = nil;
    if (self && [NSJSONSerialization isValidJSONObject:self]) {
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                         error:&error];
        if (!error && data) {
            result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        } else {
            
        }
    }
    return result;
}


+ (NSDictionary *)st_dictFromJson:(NSString *)jsonString {
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"%@",err);
        return nil;
    }
    return dic;
}


@end


