//
//  SectionModel.m
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import "SectionModel.h"

@implementation SectionModel

+ (instancetype)modelWithItems:(NSArray<id<STItem>> *)items {
    SectionModel * sectionModel = [SectionModel new];
    sectionModel.items = [NSMutableArray arrayWithArray:items];
    return sectionModel;
}

+ (instancetype)singleSectionWithItems:(nullable NSArray *)items {
    SectionModel * sectionModel = [SectionModel modelWithItems:items];
    sectionModel.isSingleSection = YES;
    return sectionModel;
}

+ (NSArray <SectionModel *>*)singleSectionArrWithItems:(nullable NSArray *)items {
    SectionModel * sectionModel = [SectionModel singleSectionWithItems:items];
    return @[sectionModel];
}



- (NSMutableArray<id<STItem>> *)items {
    if (!_items) {
        _items = [NSMutableArray new];
    }
    return _items;
}

@end


