//
//  ST_Refresh.m
//  STBasic_Objc
//
//  Created by 244235126@qq.com on 2021/8/2.
//

#import "ST_Refresher.h"
#import "NSMutableDictionary+STBase.h"
#import "STDebug.h"


@interface ST_Refresher ()

@property(nonatomic, assign, readwrite) NSInteger currentPage;

@property(nonatomic, strong, readwrite) NSArray <SectionModel *>*tempSourceItems;


/*================================================[必须设置]================================================*/

///加载数据时的请求接口  必要
@property (nonatomic, copy) List_Api requestApi;

///加载数据时的请求参数 （需要传参数的。必要） 非必须
@property (nonatomic, copy) List_Parms requestParms;

/**
 # 数据请求完成后。确认是列表数据的 list  返回NSArray。 必须。例：return @[];
 # typedef NSArray <SectionModel *>*_Nullable(^List_Objs)(NSDictionary * _Nullable responseDic);
 */
@property (nonatomic, copy) List_Objs itemList;


@property (nonatomic, copy) ComplectBlock complectBlock;

@property(nonatomic, assign) RefreshNetWorkType requestType;


@property(nonatomic, assign) BOOL pageable;



@end



static NSString *     kNetClassName_listRefresh = nil;
static NSString * kPageKey = @"Page";
static NSString * kPageSizeKey = @"Limit";

@implementation ST_Refresher

+ (void)configPageKeyForPage:(NSString *)pageKey pagesize:(NSString *)pageSizeKey {
    if (pageKey) {kPageKey = pageKey;}
    if (pageSizeKey) {kPageSizeKey = pageSizeKey;}
}

+ (void)configAppNetworkClass:(Class)mClass {
    kNetClassName_listRefresh = NSStringFromClass(mClass);
    LDLog(@"网络请求配置 mClass = %@",kNetClassName_listRefresh);
}

+ (Class<STListRefreshNetWork>)appNetworkClass {
    Class<STListRefreshNetWork> class = NSClassFromString(kNetClassName_listRefresh);
    return class;
}


+(instancetype)refresherWithApi:(List_Api)api params:(List_Parms)params objs:(List_Objs)objs requestType:(RefreshNetWorkType)requestType complect:(nullable ComplectBlock)complectBlock pageable:(BOOL)pageable {
    ST_Refresher * temp = [ST_Refresher new];
    temp.requestApi = api;
    temp.requestParms = params;
    temp.itemList = objs;
    temp.requestType = requestType;
    temp.complectBlock = complectBlock;
    temp.pageable = pageable;
    
    return temp;
}


- (RefreshNetWorkType)requestType {
    if (!_requestType) {
        _requestType = RefreshNetWorkType_Post;
    }
    return _requestType;
}


- (NSInteger)page_size {
    if (self.infiniteLoading) {
        _page_size = 0;
    } else if (!_page_size) {
        _page_size = 10;
    }
    return _page_size;
}
- (NSInteger)currentPage {
    if (!_currentPage) {
        _currentPage = 1;
    }
    return _currentPage;
}


- (NSArray<SectionModel *> *)tempSourceItems {
    if (!_tempSourceItems) {
        _tempSourceItems = [NSArray new];
    }
    return _tempSourceItems;
}

- (void)listRefreshData {
    [self loadDataAddMore:NO];
    LDLogFunction
}

- (void)listAddMoreData {
    [self loadDataAddMore:YES];
    LDLogFunction
}


- (void)loadDataAddMore:(BOOL)addMore {
    
    if (self.loadingHandle) {self.loadingHandle(YES, NO);}
    
    NSString * api = self.requestApi?self.requestApi(addMore):nil;
    NSDictionary * parms = self.requestParms?self.requestParms([NSMutableDictionary new],addMore):[NSMutableDictionary new];
    [self addMoreData:addMore apiName:api parms:parms requestType:self.requestType];
}

- (void)addMoreData:(BOOL)addMore apiName:(NSString *)apiStr parms:(NSDictionary *)requestParms requestType:(RefreshNetWorkType)requestType {

    __weak typeof(self) bself = self;

    if (!addMore && !self.reverse) {
       bself.currentPage = 1;
    }

    Class netManagerClass = NSClassFromString(kNetClassName_listRefresh);
    if ([netManagerClass conformsToProtocol:@protocol(STListRefreshNetWork)]) {

        if (!apiStr) {
            NSArray * arr = @[];
            if (bself.itemList) {
                arr = bself.itemList(nil,nil);
            }
            
            if (![arr isKindOfClass:[NSArray class]]) {
                arr = @[];
            }
            
            self.tempSourceItems = arr;
            if (bself.complectBlock) {
                bself.complectBlock(addMore,YES,bself,arr);
                bself.currentPage += 1;
            }
            return;
        }
        
        if ([apiStr containsString:ListDebugApi]) {
            [ST_Refresher fakeDataMaxCount:self.page_size handle:^(id  _Nullable data, id  _Nullable originData) {
                NSArray * arr = @[];
                if (bself.itemList) {
                    arr = bself.itemList(data,originData);
                }
                if (![arr isKindOfClass:[NSArray class]]) {
                    arr = @[];
                }
                bself.tempSourceItems = arr;
                if (bself.complectBlock) {
                    bself.complectBlock(addMore,YES,bself,arr);
                    bself.currentPage += 1;
                }
            }];
            return;
        }
        

        NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:requestParms];
        [params sl_setObject:@(self.page_size) forKey:kPageSizeKey];
        [params sl_setObject:@(self.currentPage) forKey:kPageKey];

        [[self.class appNetworkClass] refreshNetWithApiName:apiStr parms:params requestType:requestType successBlock:^(id _Nullable data,id _Nullable originData) {
            
            NSArray * arr = @[];
            if (bself.itemList) {
                arr = bself.itemList(data,originData);
            }
            
            if (![arr isKindOfClass:[NSArray class]]) {
                arr = @[];
            }
            
            bself.tempSourceItems = arr;
            if (bself.complectBlock) {
                bself.complectBlock(addMore,YES,bself,arr);
                bself.currentPage += 1;
            }
            
        } failureBlock:^(NSError * _Nullable failure) {
            if (bself.complectBlock) {
                bself.complectBlock(addMore,NO,bself,@[]);
            }
        }];
        
    } else {
        if (bself.complectBlock) {bself.complectBlock(addMore,NO,bself,@[]);}
    }
}


- (void)complectLoad {
    
}




- (void)dealloc {
    LDLog(@"%d \t %s ====== %@",(int)__LINE__, __func__,NSStringFromClass([self class]));
}




//MARK: - 🚪🚪🚪🚪 模拟的假数据
+ (void)fakeDataMaxCount:(NSInteger)count handle:(RefreshNetComplectBlock)complectBlock {
    
    // 0-无数据    1-返回数据(整页10条)  2-返回数据(不够10条)  3-网络异常
    int status = arc4random()%6;
    
    NSMutableDictionary * dataDic = [NSMutableDictionary new];
    
    NSMutableArray * itemArr = [NSMutableArray new];
    
    NSInteger backCount = count;
    
    NSString * codeKey = @"code";
    NSString * msgKey = @"msg";
    NSString * dataKey = @"data";
    NSString * listKey = @"List";
    
    if (status == 0) {
        backCount = 0;
        dataDic[codeKey] = @"-1";
        dataDic[msgKey] = @"网络请求异常状态，无法响应请求";
        LDLog(@"请求异常");
    } else if (status == 1) {
        backCount = count/2 + 1;
        dataDic[codeKey] = @"200";
        LDLog(@"数据已加载完  没有-无上拉加载");
    } else if (status == 2) {
        backCount = 0;
        LDLog(@"下拉-无数据。上拉-无更多");
    } else {
        backCount = count;
        dataDic[codeKey] = @"200";
        LDLog(@"正常分页");
    }
        

    {
        for (int i = 0; i < backCount; i ++) {
            [itemArr addObject:@{}];
        }
    }

    NSDictionary * data = @{
        listKey:itemArr
    };
    
    dataDic[dataKey] = data;
    if (complectBlock) {
        complectBlock(data,dataDic);
    }
}



@end

