
#import "STChatManager.h"
#import <BGFMDB/BGFMDB.h>

#ifdef DEBUG

#define NSLog(FORMAT, ...) fprintf(stderr,"--------------------------------------------------------------------------\n%s>%s:%d\t%s\n",__TIME__,[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#else

#define NSLog(...)

#endif


@interface STMessageTask : NSObject

@property (nonatomic , strong) STChatMessage * message;

@property (nonatomic , copy) _Nullable STChatKitSendMessageComplectHandle complect;


+ (instancetype)taskWithMessage:(STChatMessage *)message complect:(STChatKitSendMessageComplectHandle)complect;

@end



@implementation STMessageTask

+ (instancetype)taskWithMessage:(STChatMessage *)message complect:(STChatKitSendMessageComplectHandle)complect {
    STMessageTask * task = [STMessageTask new];
    task.message = message;
    task.complect = complect;
    
    return task;
}

+ (void)addTask:(STMessageTask *)task {
    [[STMessageTask taskList] addObject:task];
    if ([STMessageTask taskList].count == 1) {
        [self startTask];
    }
}


+ (void)startTask {
    STMessageTask * task = [STMessageTask taskList].firstObject;
    
    STChatMessage * message = task.message;
    STChatKitSendMessageComplectHandle complect = task.complect;
    
    __weak typeof(self) bself = self;
    
    if (![STChatManager networkClass]) {
        
        message.complectType = ChatSendComplectTypeFail;
        [message saveMsgToTable];
        if (message.msgType == STChatMessageTypeGift) {
            if (complect) {complect(message,nil);}
        }
        if (message.updateHandle) {message.updateHandle(message);}
        
        
        [[STMessageTask taskList] removeObject:task];
        
        if ([STMessageTask taskList].count > 0) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [bself startTask];
            });
            
        }
        
        return;
    }
    
    [[STChatManager networkClass] sendMsg:message complect:^(BOOL suc) {
        
     
        if (suc) {
            message.complectType = ChatSendComplectTypeOK;
            [message saveMsgToTable];
            
            STChatSession * session = [STChatSession sessionWithLastMsg:message];
            [session saveToTable];
            
            if (message.msgType == STChatMessageTypeGift) {
                if (complect) {complect(message,nil);}
            }
            
            if (message.updateHandle) {message.updateHandle(message);}
            
        } else {
            
            message.complectType = ChatSendComplectTypeFail;
            [message saveMsgToTable];
            if (message.msgType == STChatMessageTypeGift) {
                if (complect) {complect(message,nil);}
            }
            if (message.updateHandle) {message.updateHandle(message);}
            
        }
        
        [[STMessageTask taskList] removeObject:task];
        
        if ([STMessageTask taskList].count > 0) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [bself startTask];
            });
            
        }
        
    } loadingHud:NO errorTip:NO];
}

+ (NSMutableArray <STMessageTask*>*)taskList {
    static NSMutableArray * tList = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tList = [[NSMutableArray alloc] init];
    });

    return tList;
}

@end


@interface STChatManager ()

@end


@implementation STChatManager (NetWork)


static NSString * kNetClassName_Chat = nil;
+ (void)configChatKitNetworkClass:(Class)mClass {
    kNetClassName_Chat = NSStringFromClass(mClass);
    NSLog(@"STChatKit网络请求配置 mClass = %@",kNetClassName_Chat);
}

+ (Class<STChatKitProrocol>)networkClass {
    Class<STChatKitProrocol> class = NSClassFromString(kNetClassName_Chat);;
    return class;
}


+ (void)uploadImages:(NSArray<UIImage *> * _Nullable)items loading:(BOOL)loading success:(nullable void(^)(NSArray <NSString *> *urlStrs))success fail:(nullable void(^)(NSError * error))fail {
    
    Class<SDKNetworkingProtocal> class = [self networkClass];
    
    if ([class conformsToProtocol:@protocol(SDKNetworkingProtocal)]) {
        [class uploadImages:items loading:loading success:success fail:fail];
    }
    
}


+ (void)appNetworkSendMessage:(STChatMessage *)message complect:(nullable STChatKitSendMessageComplectHandle)complect {
    
    STMessageTask * task = [STMessageTask taskWithMessage:message complect:complect];
    [STMessageTask addTask:task];
    
}


@end


@implementation STChatManager

+ (NSString *)chatUid {
    return [[self networkClass] appUserId];
}

//MARK: 发送消息
+ (void)sendMessage:(STChatMessage *)message toUser:(NSString *)toUserId complect:(nullable STChatKitSendMessageComplectHandle)complect {
    
    if (![self chatUid]) {return;}
    
    message.fromUid = [self chatUid];
    message.toUid = toUserId;
    message.channelId = [STChatManager channelWithUid:toUserId];
    message.createTimeSpan = [[NSDate date] timeIntervalSince1970];
    message.updateTimeSpan = [[NSDate date] timeIntervalSince1970];
    
    if (message.msgType == STChatMessageTypeImage) {
        
        {
            message.complectType = ChatSendComplectTypeSending;
            ///暂存消息
            [message saveMsgToTable];
            
            ///更新会话
            STChatSession * session = [STChatSession sessionWithLastMsg:message];
            session.unReadCount = 0;
            [session saveToTable];
            if (complect) {complect(message,nil);}
        }
        
        if (message.imageObj.urlString) {
            [STChatManager appNetworkSendMessage:message complect:complect];
        } else if (message.imageObj.localImage) {
            [STChatManager uploadImages:@[message.imageObj.localImage] loading:NO success:^(NSArray<NSString *> *urlStrs) {
                
                message.imageObj.localImage = nil;
                message.imageObj.urlString = urlStrs.firstObject;
                [STChatManager appNetworkSendMessage:message complect:complect];
                
            } fail:^(NSError *error) {
                
                message.complectType = ChatSendComplectTypeFail;
                [message saveMsgToTable];
                if (message.updateHandle) {message.updateHandle(message);}
                
            }];
        }
        
    } else if (message.msgType == STChatMessageTypeText || message.msgType == STChatMessageTypeGift) {
        
        message.complectType = ChatSendComplectTypeSending;
        
        ///更新会话
        STChatSession * session = [STChatSession sessionWithLastMsg:message];
        session.unReadCount = 0;
        
        
        if (message.msgType == STChatMessageTypeGift) {
            
        } else {
            ///暂存消息
            [message saveMsgToTable];
            [session saveToTable];
            if (complect) {complect(message,nil);}
        }
        
        [STChatManager appNetworkSendMessage:message complect:complect];
    }
    
}
@end




@implementation STChatManager (Helper)
+ (NSString *)messageContent:(STChatMessage *)message {
    
    Class<STChatKitProrocol> class = [self networkClass];
    if ([class conformsToProtocol:@protocol(STChatKitProrocol)]) {
        return [class messageContentText:message];
    }
    return @"";
}


+ (NSString *)channelWithUid:(NSString *)peer {
    if ([STChatManager.chatUid integerValue] < [peer integerValue]) {
        return [NSString stringWithFormat:@"%@_%@",STChatManager.chatUid,peer];
    }
    return [NSString stringWithFormat:@"%@_%@",peer,STChatManager.chatUid];
    
}

+ (NSString *)newIdentifer {
    NSString * identifer = [NSString stringWithFormat:@"identifer_%zd",(NSInteger)[[NSDate new] timeIntervalSince1970] * 1000];
    return identifer;
}


#pragma mark 辅助方法
+ (void)tableView:(UITableView *)tableView addMessage:(STChatMessage *)message {
    if (!message) return;
    [tableView.singleListItems addObject:message];
    NSIndexPath * indexpath = [NSIndexPath indexPathForRow:(tableView.singleListItems.count - 1) inSection:0];
    if (indexpath.row > 2) {
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationBottom];
        [tableView endUpdates];
        [tableView scrollToRowAtIndexPath:indexpath atScrollPosition:(UITableViewScrollPositionBottom) animated:YES];
    } else {
        [tableView reloadData];
    }
}

+ (void)configChatTableView:(UITableView *)tableView pageSize:(NSInteger)pageSize firstLoad:(BOOL)firstLoad targetUid:(NSString *)targetUid itemCellClass:(ItemMsgHandle)itemClassHandle {
    
    __weak typeof(tableView) btableView = tableView;
    
    [tableView ST_ListDefaultSetting];
    
    if (firstLoad) {
        tableView.enableHead = YES;
        NSString * channelId = [STChatManager channelWithUid:targetUid];
        [tableView setRefresherWithApi:nil params:nil objs:^NSArray<SectionModel *> * _Nullable(NSDictionary * _Nullable data,NSDictionary * _Nullable responseDic) {
            
            STChatMessage * firstMsg = (STChatMessage *)btableView.singleListItems.firstObject;
            NSArray * models = [STChatMessage getMessageWithCount:pageSize channelId:channelId firstMessage:firstMsg complect:nil];
            
            return models;
            
        } requestType:(RefreshNetWorkType_None) pageable:YES];
        
        tableView.st_refresh.reverse = YES;
        tableView.st_refresh.page_size = pageSize;
        [tableView startHeadRefreshWithAnimation:NO];
        
    } else {
        [tableView reloadData];
    }
    
    tableView.itemClassHandle = itemClassHandle;
    
    NSString * identifer = [STChatManager newIdentifer];
    tableView.identifer = identifer;
    
}


///对方-已读回执处理
+ (void)receiverHasReceiver:(NSString *)chatUid handle:(nullable void(^)(BOOL success))handle {
    
    
    //1.获取已发送的未读消息
    NSString * where = [NSString stringWithFormat:@"where %@=%@ and %@=%@ and %@=%@",bg_sqlKey(@"fromUid"),bg_sqlValue([self chatUid]),bg_sqlKey(@"toUid"),bg_sqlValue(chatUid),bg_sqlKey(@"isRead"),bg_sqlValue(@(0))];
    
    NSArray <STChatMessage *>* unreadArr = [STChatMessage bg_find:[STChatMessage tableNameNameWithPeer:chatUid] where:where];
    if (!unreadArr.count) {
        return;
    }
    
    //2.将未读消息标记为已读消息
    [unreadArr enumerateObjectsUsingBlock:^(STChatMessage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isRead = YES;
    }];
    
    //3.更新数据库
    [STChatMessage bg_saveOrUpdateArrayAsync:unreadArr complete:^(BOOL isSuccess) {
        if (handle) {handle(isSuccess);}
    }];
}


@end




@implementation STChatManager (Debug)

+ (NSArray <NSString *> *)debug_textList {
    NSArray <NSString *>* arr = @[
        @"1秒，2秒，3秒……几家北京信创企业的“码农”围坐在一台笔记本电脑前，屏住呼吸，全神贯注，心脏仿佛都随秒针同频跳动。",
        @"到了第14秒，电脑桌面顺利“点亮”，实验室里顿时一片欢腾。",
        @"从各自为战，到攥指成拳，统信联合产业上下游企业，协同推进国产操作系统研发。\n技术快速迭代中，国产操作系统开机时长一步步缩短，终于在2022年实现14秒飞跃。",
        @"2022年3月，搭载着龙芯3A5000 4核处理器、统信UOS操作系统、昆仑BIOS固件的同方笔记本电脑实现14秒开机时长的飞跃性突破，代表着“中国芯”“中国魂”的计算机正从“可用”迈向“好用”，在英特尔、微软占据多年的信息技术产业江湖里拓展着中国人的一席之地。",
        @"向“14秒”飞跃的每一行代码里，暗藏着几代“北京码农”的创意与雄心。代表北京“软实力”的他们，有的已是奋战几十年的白发先生，有的还是刚刚入行的翩翩少年，信息技术应用创新链条上的一道道难关，正在他们的并肩奋战中取得关键性突破。",
        @"龙芯CPU走出“至暗时刻\n"
        "开机速度是计算机性能给使用者留下的第一印象。10年前，打开一台搭载着国产硬件、软件的计算机，需要10分钟、甚至更长时间。\n"
        "CPU（中央处理器）是计算机的心脏，想要给计算机提速，势必先过这一关。中国人有自己的CPU吗？",
        @"有。",
        @"2002年8月10日凌晨，安装着“龙芯1号”CPU的计算机成功启动，终结了中国人完全依靠进口CPU制造计算机的历史。2010年，中科院与北京市携手出资成立龙芯中科公司，期望研发成果加速产业化落地。",
        @"但是，仅过了两年多，龙芯中科便陷入“至暗时刻”。",
        @"学术味很浓的龙芯团队一直将目光聚焦在提升CPU性能上，迟迟没有拿出符合市场主流需求的通用CPU。研发的高投入，让公司资金吃紧，甚至一度开不出工资，导致人才流失。",
        @"“这次危机逼着我们认清了现实。”创始人胡伟武意识到，“下海”创业的龙芯研发人员放弃了中科院的编制，却忘了关注市场的真正需求。也就是说，组织上虽然转型了，但观念还没有跟上。",
        @"中科院计算技术研究所总工程师、龙芯总设计师胡伟武",
        @"当时，市场主流CPU是多核设计，英特尔主打双核、四核，龙芯为了技高一筹，做起了八核。胡伟武坦言，“人多力量大”的前提是每个人都得强，龙芯虽然是八核，但每核的性能都不如人。龙芯的CPU在当时不被市场接受，原因很简单——不好用",
        @"走了几年的弯路，龙芯终于放下“身段”，将目光投向工业控制领域CPU，之后又在信息化领域成功应用。在不断试错和迭代中，龙芯在2015年收入破亿元，首次实现盈亏平衡。",
        @"2015年到2020年，龙芯性能提高了10倍，这才有了支撑14秒开机的可能。开机时长缩短至14秒的同方电脑，所搭载的CPU正是龙芯3A5000系列，其性能已经逼近开放市场CPU的主流水平。",
        @"国产操作系统初长成",
        @"对于广大计算机用户来说，Windows操作系统的经典图标深深刻在了记忆中。CPU是电脑的心脏，操作系统则是灵魂。如果说龙芯对标的是英特尔，那么谁能对标微软？",
        @"2011年，当胡伟武正在为更高水平的CPU奋力一搏时，统信软件公司总经理刘闻欢决定从头创业，打造国产操作系统。",
        @"“如果把攻克芯片技术比作攀登喜马拉雅山，解决国产操作系统就是探索马里亚纳海沟。”在此之前，他在一家信息安全企业工作了十几年，愈发感觉到倘若解决不了操作系统瓶颈，信息安全只能是纸上谈兵。",
        @"同一时期，中国诞生了一批抱着同样理想的软件公司。但理想丰满，现实骨感，第一波尝鲜的人发现，搭载国产操作系统的计算机不仅速度慢，更无法如Windows一样正常聊天、办公、打游戏。虽然不断有国产新系统上线，但面对技术的差距和市场的残酷选择，这些软件企业接二连三地倒下。",
        @"攻克难关无法靠一己之力解决。要解决开机慢、续航短这些用户痛点，必须CPU、操作系统等行业联合攻关。"
        "2019年，国内多个操作系统厂家联合组建的统信软件技术有限公司成立，总部就设在北京经开区信创园，并在武汉、上海、广州等多地设立技术支持机构、研发中心和通用软硬件适配中心。",
    ];
    
    return arr;
}

+ (NSString *)debug_text {
    
    NSArray <NSString *>* arr = [self debug_textList];
    NSInteger index = arc4random()%arr.count;
    
    return arr[index];
}

+ (void)debug_sendMsgsCount:(int)msgCount toUid:(NSString *)uid showInTableView:(UITableView *)tableView {
    
    NSArray <NSString *>* arr = [self debug_textList];
    
    for (int i=0; i<msgCount; i++) {
        
        
        NSThread * thread = [[NSThread alloc] initWithBlock:^{
            
        }];
        
        NSInteger index = arc4random()%arr.count;
        NSString * text = arr[index];
        
        STChatMessage * msg = [STChatMessage messageWithText:text];
        [STChatManager sendMessage:msg toUser:uid complect:^(STChatMessage * _Nullable message, id  _Nullable data) {
            [STChatManager tableView:tableView addMessage:message];
        }];
        
    }
    
}

@end
