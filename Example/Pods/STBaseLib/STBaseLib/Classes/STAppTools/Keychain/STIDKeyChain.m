
#import "STIDKeyChain.h"
#import "STDebug.h"
#import "NSString+STBase.h"

static NSString * KEY_UDID_INSTEAD = nil;

@implementation STIDKeyChain

+ (NSString *)KEY_UDID_INSTEADString {
    
    if (!KEY_UDID_INSTEAD) {
        KEY_UDID_INSTEAD = [NSString appBundleID];
    }
    
    return KEY_UDID_INSTEAD;
}

+(BOOL)checkExistForkey:(NSString *)key {
    NSString *findUDIDInKeychain = (NSString *)[STIDKeyChain load:key];
    if (!findUDIDInKeychain ||[findUDIDInKeychain isEqualToString:@""]||[findUDIDInKeychain isKindOfClass:[NSNull class]]) {
        return NO;
    }else{
        return YES;
    }
}

+(NSString *)getDeviceIDInKeychain {
    return [self randomIDFOrkey:[self KEY_UDID_INSTEADString]];
}



+ (void)TestdeleteServer{
    [self delete:[self KEY_UDID_INSTEADString]];
}



+ (NSString *)randomIDFOrkey:(NSString *)key {
    NSString *findUDIDInKeychain = (NSString *)[STIDKeyChain load:key];
    if (!findUDIDInKeychain ||[findUDIDInKeychain isEqualToString:@""]||[findUDIDInKeychain isKindOfClass:[NSNull class]]) {
        NSString * result = [self createNewID];
        [STIDKeyChain save:key data:result];
        findUDIDInKeychain = result;
    }
    LDLog(@"获取到的-UDID %@",findUDIDInKeychain);
    return findUDIDInKeychain;
}





#pragma mark KeyChain
+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service {
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (id)kSecClassGenericPassword,(id)kSecClass,
            service, (id)kSecAttrService,
            service, (id)kSecAttrAccount,
            (id)kSecAttrAccessibleAfterFirstUnlock,(id)kSecAttrAccessible,
            nil];
}

+ (void)save:(NSString *)service data:(id)data {
    //Get search dictionary
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    //Delete old item before add new item
    SecItemDelete((CFDictionaryRef)keychainQuery);
    //Add new object to search dictionary(Attention:the data format)
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(id)kSecValueData];
    //Add item to keychain with the search dictionary
    SecItemAdd((CFDictionaryRef)keychainQuery, NULL);
    
    if (service && data) {
        LDLog(@"保存keychain %@",@{service:data});
    }
}


+ (id)load:(NSString *)service {
    id ret = nil;
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    //Configure the search setting
    //Since in our simple case we are expecting only a single attribute to be returned (the password) we can set the attribute kSecReturnData to kCFBooleanTrue
    [keychainQuery setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
    [keychainQuery setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    CFDataRef keyData = NULL;
    if (SecItemCopyMatching((CFDictionaryRef)keychainQuery, (CFTypeRef *)&keyData) == noErr) {
        @try {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)keyData];
        } @catch (NSException *e) {
            LDLog(@"Unarchive of %@ failed: %@", service, e);
        } @finally {
        }
    }
    if (keyData)
        CFRelease(keyData);
    return ret;
}

+ (void)delete:(NSString *)service {
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    SecItemDelete((CFDictionaryRef)keychainQuery);
}



+ (NSString *)createNewID {
    CFUUIDRef puuid = CFUUIDCreate( nil );
    CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
    NSString * result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, uuidString));
    CFRelease(puuid);
    CFRelease(uuidString);
    
    LDLog(@"\n 新建UUID _____\n  %@",result);
    
    return result;
}











@end
