

#import "STTableCell.h"

@implementation STTableCell
@synthesize itemCell_TempModel = _itemCell_TempModel;

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setItemCell_TempModel:(id)itemCell_TempModel {
    _itemCell_TempModel = itemCell_TempModel;
}

@end
