//
//  UIDevice+STBase.h
//  LDSDK
//
//  Created by 石磊 on 2023/2/9.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (STBase)

///设备屏幕宽度
+ (CGFloat)width;

///设备屏幕高度
+ (CGFloat)height;

///设备唯一ID  重装系统会更行
+ (NSString *)deviceId;

///手动重置设备ID
+ (void)changeDeviceId;


///跳转应用设置页面
+ (void)jumpToAppSettings;

///跳转应用市场
+ (void)jumpToAppStoreWithAppID:(NSString *)appID complect:(void (^ __nullable)(BOOL success))complect;

///跳转应用评价
+ (void)jumpToAppStoreEVAWithAppID:(NSString *)appID complect:(void (^ __nullable)(BOOL success))complect;


///跳转外部浏览器
+ (void)jumpToOutUrlstring:(NSString *)urlString;

@end

NS_ASSUME_NONNULL_END
