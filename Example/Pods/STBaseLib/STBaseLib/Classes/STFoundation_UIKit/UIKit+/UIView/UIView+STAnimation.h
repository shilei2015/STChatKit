//
//  UIView+STAnimation.h
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (STAnimation)

///缩放动画
- (void)showExpandAnimation;

- (void)removeAnimation;

@end


@interface STDiffusionAniView : UIView

///扩散线条宽度 默认 2
@property (nonatomic , assign) IBInspectable NSInteger diffusionWidth;

///扩散线条颜色 默认 orange
@property (nonatomic , strong) IBInspectable UIColor * diffusionColor;

///扩散线条条数 默认 3
@property (nonatomic , assign) IBInspectable NSInteger diffusionCount;

///扩散线条动画时间 默认 3s
@property (nonatomic , assign) IBInspectable double diffusionDuration;

@property (nonatomic , assign) BOOL needBGColor;




@property (nonatomic , assign) BOOL needBorder;

@property (nonatomic , assign) float diffusionScale;


- (void)startAnimation;

- (void)stopAnimation;

@end

NS_ASSUME_NONNULL_END
