

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STBlurView : UIVisualEffectView

///自定义模糊度
@property (nonatomic, assign) IBInspectable CGFloat customIntensity;

- (void)updateConfig;

@end

NS_ASSUME_NONNULL_END
