//
//  NSObject+STBase.h
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import <Foundation/Foundation.h>
#import "STBaseDefine.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (STBase)

@property(nonatomic, copy) STParentVCHandle parentController;

@property (nonatomic,strong) id tempModel;

@property (nonatomic , copy) _Nullable HandleBlock defaultHandleBlock;

///配置默认回调
- (void)configHandleBlock:(nullable HandleBlock)handleBlock;

/////获取默认回调
//- (nullable HandleBlock)handleBlock;

- (void)handleBackType:(NSInteger)type obj:(nullable id)obj des:(nullable NSString *)des;

/// 添加一个回调
/// - Parameters:
///   - handle: 回调
///   - identifier: 回调标记
- (void)addHandle:(HandleBlock)handle withIdentifer:(NSString *)identifier;


/// 根据identifier获取对应的回调
/// - Parameter identifier: 回调标记
- (HandleBlock)handleWithIdentifier:(NSString *)identifier;



/// 一定时间内防止重复出发事件
/// - Parameters:
///   - inTime: 防重复时间
///   - handle: 事件回调
- (void)resetEventTimeInTime:(NSTimeInterval)inTime handle:(HandleBlock)handle;


/// 重复执行 时间间隔为time 回调doSome
- (void)st_everyTime:(NSTimeInterval)time doSome:(void(^)(void))doSome;


///
- (void)cancelAllEnvent;

/// 交换方法用户hook系统方法或现有方法
/// - Parameters:
///   - originSel: 原有方法
///   - exchangeSel: 交换方法
+ (void)exchangeSelector:(SEL)originSel enchangeSel:(SEL)exchangeSel;


@end


@interface NSObject (STArchive)

/**  归档  */
- (BOOL)saveToPath:(NSString *)path;

/**  解档  */
+ (NSObject *)objectAtPath:(NSString *)path;

/** 删除 */
+ (BOOL)deleteItemAtPath:(NSString *)path;


@end


@interface NSObject (Keyboard)

+ (void)configKeyboardWithBottomCons:(NSLayoutConstraint *)bottomCons layoutView:(UIView *)layoutView;

+ (void)configKeyboardWithBottomCons:(NSLayoutConstraint *)bottomCons layoutView:(UIView *)layoutView showMargin:(CGFloat)showMargin;

@end



NS_ASSUME_NONNULL_END


@interface EndMark : NSObject

@end


@interface NSObject (Utils)

+ (id _Nullable )performSelector:(SEL _Nullable )aSelector withObjects:(id _Nullable )object, ... ;

@end
