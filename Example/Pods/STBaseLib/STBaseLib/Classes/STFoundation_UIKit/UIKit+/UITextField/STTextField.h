//
//  STTextField.h
//  STBaseLib
//
//  Created by EDY on 2023/6/14.
//

#import <UIKit/UIKit.h>
@class STTextField;

NS_ASSUME_NONNULL_BEGIN

static NSString * const kRegEmail = @"^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";


static NSString * const kReg8_20Text = @"^.{8,20}$";

static NSString * const kReg4Number = @"^\\d{4}$";


static NSString * const kRegOnlyNumber = @"^[0-9]+$";
static NSString * const kRegtemp = @"";






typedef void(^STTextFieldHandle)(STTextField * tf);

@interface STTextField : UITextField<UITextFieldDelegate>

///最大位数限制
@property (nonatomic , assign) NSInteger maxLength;

///格式判断条件的正则
@property (nonatomic , copy) NSString * regularFormat;

///输入格式判断条件的正则
@property (nonatomic , copy) NSString * regularTypeIn;

///验证通过
@property (nonatomic , assign , readonly) BOOL regularOK;

///内容发生改变  - 触发
@property (nonatomic , copy) STTextFieldHandle textChanged;


///点击键盘上的return按钮 - 触发
@property (nonatomic , copy) STTextFieldHandle nextAction;




@end

NS_ASSUME_NONNULL_END
