//
//  NSDate+STBase.h
//  STTempPods
//
//  Created by 石磊 on 2022/4/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (STBase)


#pragma mark - 返回NSString部分

/// NSDate转格式显示
/// @param date 例如：[NSDate date]
/// @param formatter @"yyyy-MM-dd"
+ (NSString *)dateToString:(NSDate *)date dateFormatter:(NSString *)formatter;

/// 时间戳转格式显示
+ (NSString *)timeIntervalToString:(NSTimeInterval)timeInterval dateFormatter:(NSString *)formatter;

+ (NSString *)timeSpan;

+ (NSString *)timeSpanMS;

#pragma mark - 返回NSDate部分

/// 根据显示时间的字符串返回NSDate
/// @param timeString 2020-01-01
/// @param format @"yyyy-MM-dd"
+ (NSDate *)dateWithDateString:(NSString *)timeString formatString:(NSString *)format;

/// 在已有NSDate的情况下，加减具体数字的年月日，获取未来或者过去的NSDate
+ (NSDate *)dateWithPresentDate:(NSDate*)presentDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;

/// 根据具体的年月日获取NSDate
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;

#pragma mark - 返回整型部分
/// 根据前后NSDate，获取中间差了多少天
+ (NSInteger)daysBetweenStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;
/// 根据前后NSDate，获取中间差了多少月
+ (NSInteger)monthBetweenStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;
/// 根据前后NSDate，获取中间差了多少年
+ (NSInteger)yearsBetweenStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;

/// 获取传入Date对应的月份有多少天
+ (NSInteger)totaldaysInMonth:(NSDate *)date;

//// 是否和当前同一天
//+ (BOOL)isCurrentDay:(NSDate *)date;


///判断同一天
+ (BOOL)inSameDayWithDate:(NSDate *)date otherDate:(NSDate *)otherDate;

@end

NS_ASSUME_NONNULL_END
