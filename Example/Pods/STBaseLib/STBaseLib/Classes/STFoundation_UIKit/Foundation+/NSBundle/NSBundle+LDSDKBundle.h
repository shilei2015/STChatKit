//
//  NSBundle+LDSDKBundle.h
//  SDKTestProject
//
//  Created by 石磊 on 2023/1/5.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@protocol SDKBundleProtocal <NSObject>

+ (NSString *)bundleName;

@end



@interface NSBundle (LDSDKBundle)



/// 获取bundle
/// @param bundleName bundle name
+ (instancetype)st_SDKBundle:(nullable NSString *)bundleName;


/// 获取国际化后的文本
/// @param key 代表 Localizable.strings 文件中 key-value 中的 key。
/// @param language 设置语言（可为空，为nil时将随系统的语言自动改变）
+ (NSString *)ld_localizedStringForKey:(NSString *)key language:(NSString *)language;

@end






NS_ASSUME_NONNULL_END
