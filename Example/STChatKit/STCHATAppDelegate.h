//
//  STCHATAppDelegate.h
//  STChatKit
//
//  Created by shilei2015 on 07/12/2023.
//  Copyright (c) 2023 shilei2015. All rights reserved.
//

@import UIKit;

@interface STCHATAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
