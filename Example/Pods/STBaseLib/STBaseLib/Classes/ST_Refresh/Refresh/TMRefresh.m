//
//  TMRefresh.m
//  TMM
//
//  Created by  jun on 2019/11/10.
//  Copyright © 2019 yinhe. All rights reserved.
//

#import "TMRefresh.h"

@implementation TMRefreshHeader
- (void)prepare {
    [super prepare];
    self.lastUpdatedTimeLabel.hidden = YES;
    self.stateLabel.hidden = YES;
}

- (void)placeSubviews {
    [super placeSubviews];
    
    self.loadingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    self.arrowView.tintColor = [UIColor whiteColor];
    
    self.lastUpdatedTimeLabel.hidden = YES;
    self.stateLabel.hidden = YES;
}

@end


@implementation TMRefreshFooter
- (void)prepare{
    [super prepare];
    self.stateLabel.hidden = YES;
}

- (void)placeSubviews{
    [super placeSubviews];

    self.loadingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    self.arrowView.tintColor = [UIColor whiteColor];
}

@end
