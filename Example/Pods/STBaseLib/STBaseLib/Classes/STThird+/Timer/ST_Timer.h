//
//  STTimer.h
//  LDSDK
//
//  Created by 石磊 on 2023/2/3.
//

#import <Foundation/Foundation.h>

typedef struct {
    NSInteger day;
    NSInteger hour;
    NSInteger minute;
    NSInteger second;
    
    NSInteger total;
} STTime;


NS_ASSUME_NONNULL_BEGIN

@interface ST_Timer : NSObject

///开始计时
+ (void)startTimerFor:(NSString *)identifer handle:(void(^)(void))handle;

+ (void)removeTimerWithIdentifer:(NSString *)identifer;


///时间转换
+ (STTime)timeWithTimeInterval:(NSInteger)timeInterval;

@end

NS_ASSUME_NONNULL_END
