//
//  UIImage+STSize.m
//  Asocial
//
//  Created by 石磊 on 2022/11/2.
//

#import "UIImage+STSize.h"
#import "STDebug.h"
@implementation UIImage (STSize)

+ (float)sizeGetMaxLength:(CGSize)size {
    return size.width >= size.height ? size.width : size.height;
}

+ (float)sizeGetMinLength:(CGSize)size {
    return size.width < size.height ? size.width : size.height;
}



- (UIImage *)compressionWithMaxImageSize:(CGSize)maxImageSize {
    CGSize originSize = self.size;
    CGSize tempMaxSize;
    if (originSize.width >= originSize.height) {
        tempMaxSize = CGSizeMake([UIImage sizeGetMaxLength:maxImageSize], [UIImage sizeGetMinLength:maxImageSize]);
    } else {
        tempMaxSize = CGSizeMake([UIImage sizeGetMinLength:maxImageSize], [UIImage sizeGetMaxLength:maxImageSize]);
    }
    
    float scale_w = originSize.width / tempMaxSize.width;//
    float scale_h = originSize.height / tempMaxSize.height;//
    
    NSData *data = UIImageJPEGRepresentation(self, 1);  
    LDLog(@"图片大小为 = %02fM",data.length/1024.0/1024.0);
    LDLog(@"图片尺寸为 = CGSize(%lf,%lf)",originSize.width,originSize.height);
    
    if (scale_h > 1 || scale_w > 1) {
        float maxScale = MAX(scale_h, scale_w);
        CGSize newSize = CGSizeMake(originSize.width/maxScale, originSize.height/maxScale);
        LDLog(@"压缩 - 后图片尺寸为 = CGSize(%lf,%lf)",newSize.width,newSize.height);
        return [self st_resize:newSize];
    }
    
    /*
    目标最大尺寸 1000*1500
    现有图片尺寸 2000*2000
     压缩方案:以高倍数压缩 为1000*1000
             以低倍数压缩 为1500*1500
     
     现有图片尺寸  540 * 720(不处理[小于目标尺寸])  1440*1080(不处理[等于目标尺寸]) 1080 * 2880(疑问:尺寸压缩为540*1440)
     */
    LDLog(@"不压缩尺寸");
    return self;
}


- (UIImage *)compressImageQualityToByte:(NSInteger)maxLength {
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(self, compression);
    if (data.length < maxLength) return self;
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(self, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    
    LDLog(@"压缩 - 后图片大小为 = %02fM",data.length/1024.0/1024.0);
    
    return resultImage;
}




- (UIImage *)st_resize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [self drawInRect:CGRectMake(0,0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}



+ (UIImage *)createImageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}



@end
