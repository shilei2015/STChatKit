//
//  LDVisualEffectView.m
//  SDKTestProject
//
//  Created by 石磊 on 2023/1/5.
//

#import "STBlurView.h"
#import "STDebug.h"

@interface STBlurView ()
@property (nonatomic, strong) UIVisualEffect *theEffect;

@property (nonatomic, strong) UIViewPropertyAnimator *animator;
@end


@implementation STBlurView

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self.theEffect = self.effect;
    }
    return self;
}

- (instancetype)initWithEffect:(UIVisualEffect *)effect {
    self = [super initWithEffect:nil];
    if (self) {
        self.theEffect = effect;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {

    // Drawing code
    [super drawRect:rect];

    [self updateConfig];
}


- (void)setHidden:(BOOL)hidden {
    [super setHidden:hidden];
    if (hidden == NO) {
        [self updateConfig];
    } else {
        self.effect = nil;
        [self.animator stopAnimation:YES];
        
    }
}



- (void)updateConfig {
    self.effect = nil;
    [self.animator stopAnimation:YES];
    
    __weak typeof(self) bself = self;
    self.animator = [[UIViewPropertyAnimator alloc] initWithDuration:1 curve:UIViewAnimationCurveLinear animations:^{
        bself.effect = bself.theEffect;
    }];
    self.animator.fractionComplete = self.customIntensity;
}


- (CGFloat)customIntensity {
    return _customIntensity ?: 0.5;
}



- (void)dealloc {
    [self.animator stopAnimation:YES];
    [self subDealloc];
    NSString * logT = [NSString stringWithFormat:@"__dealloc__❤️❤️❤️❤️❤️❤️❤️❤️ %@",NSStringFromClass([self class])];
    LDLog(@"%@",logT);
}

- (void)subDealloc {}



@end
