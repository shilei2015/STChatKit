//
//  STNetworkConst.h
//  STBase
//
//  Created by 石磊 on 2022/12/19.
//

#ifndef STNetworkConst_h
#define STNetworkConst_h



typedef enum {
    NetRequestType_None,
    NetRequestType_Get,
    NetRequestType_Post,
} NetRequestType;

typedef NS_ENUM(NSUInteger, STNetworkStatus) {
    /*! 未知网络 */
    STNetworkStatusUnknown           = 0,
    /*! 没有网络 */
    STNetworkStatusNotReachable,
    /*! 手机 3G/4G 网络 */
    STNetworkStatusReachableViaWWAN,
    /*! wifi 网络 */
    STNetworkStatusReachableViaWiFi
};


/// 网络请求失败后的回调
typedef void (^FailureBlock) (NSError * _Nullable failure);

///请求完成的回调
typedef void (^NetComplectBlock) (id _Nullable responseDic,NSError * _Nullable error);


typedef void(^STNetworkStatusBlock)(STNetworkStatus status);


typedef void(^STUploadProgressBlock)(CGFloat progress);



#endif /* STNetworkConst_h */
