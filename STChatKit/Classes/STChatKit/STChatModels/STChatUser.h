//
//  STChatUser.h
//  STChatKit
//
//  Created by EDY on 2023/7/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STChatUser : NSObject

@property(nonatomic, copy) NSString *Nickname;

@property(nonatomic, copy) NSString *HeadImage;

@property(nonatomic, copy) NSString *UserId;

@property (nonatomic , assign) BOOL pined;

///创建用户模型
+ (instancetype)userWithUid:(NSString *)uid name:(NSString *)name avatar:(NSString *)avatar;

///保存用户信息到数据库
+ (void)saveUserWithUid:(NSString *)uid name:(NSString *)name avatar:(NSString *)avatar;

///根据ID查询用户
+ (nullable STChatUser *)findUserById:(NSString *)uid;


- (void)saveToTable;


@end

NS_ASSUME_NONNULL_END
