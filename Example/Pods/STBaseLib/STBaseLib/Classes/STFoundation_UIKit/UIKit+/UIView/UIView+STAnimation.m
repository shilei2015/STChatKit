//
//  UIView+STAnimation.m
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import "UIView+STAnimation.h"
#import "UIColor+STBase.h"

static NSString * const kExpandAnimationKey = @"expandAnimation";


@implementation UIView (STAnimation)

- (void)showExpandAnimation {
    [self.layer addAnimation:[UIView expandAnimation] forKey:kExpandAnimationKey];
}

- (void)removeAnimation {
    [self.layer removeAnimationForKey:kExpandAnimationKey];
}



+ (CAAnimationGroup *)expandAnimation {
    CABasicAnimation *animation1 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation1.fromValue = @1;
    animation1.toValue = @1.15;
    animation1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation1.duration = 0.3;
    animation1.beginTime = 0;

    CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation2.fromValue = @1.15;
    animation2.toValue = @1;
    animation2.duration = 0.3;
    animation2.beginTime = animation1.duration + animation1.beginTime;

    CABasicAnimation *animationEnd = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animationEnd.fromValue = @1;
    animationEnd.toValue = @1;
    animationEnd.duration = 0.2;
    animationEnd.beginTime = animation2.duration + animation2.beginTime;

    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.animations = @[animation1, animation2, animationEnd];
    animationGroup.duration = animationEnd.beginTime + animationEnd.duration;

    animationGroup.repeatCount = NSIntegerMax;
    animationGroup.autoreverses = NO;
    animationGroup.removedOnCompletion = NO;
    animationGroup.fillMode = kCAFillModeForwards;


    return animationGroup;
}

@end


@interface STDiffusionAniView ()

@property (nonatomic,strong) CALayer *animationLayer;

@property (nonatomic , assign) CGFloat itemW;

@end

@implementation STDiffusionAniView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self nibLoadSelf];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self nibLoadSelf];
    }
    return self;
}

- (void)nibLoadSelf {
    
}

- (void)startAnimation {
     
    [self stopAnimation];
    
    if (!self.animationLayer) {

        CALayer *animationLayer = [CALayer layer];
        for (int i = 0; i < self.diffusionCount; i++) {
            CAAnimationGroup * group = [self groupAniIndex:i];
            
            NSMutableArray * ttArr = [NSMutableArray new];
            [ttArr addObject:[self animate_scale]];
            [ttArr addObject:[self animate_alpha]];
            
            if (self.needBorder) {
                [ttArr addObject:[self animate_borderWidth]];
            }
            if (self.needBGColor) {
                [ttArr addObject:[self animate_backGroundColor]];
            }
               
            group.animations = ttArr;

            CALayer *pulsingLayer = [self pulsingLayer:self.frame animation:group];
            [animationLayer addSublayer:pulsingLayer];
        }
        self.animationLayer = animationLayer;
    }
    [self.layer addSublayer:self.animationLayer];
}


- (void)stopAnimation {
    [self.animationLayer removeAllAnimations];
    [self.animationLayer removeFromSuperlayer];
    self.animationLayer = nil;
}


- (NSInteger)diffusionCount {
    if (!_diffusionCount) {
        return 3;
    }
    return _diffusionCount;
}

- (double)diffusionDuration {
    if (!_diffusionDuration) {
        return 3.0;
    }
    return _diffusionDuration;
}

- (NSInteger)diffusionWidth {
    if (!_diffusionWidth) {
        return 2;
    }
    return _diffusionWidth;
}

- (UIColor *)diffusionColor {
    if (!_diffusionColor) {
        return [UIColor orangeColor];
    }
    return _diffusionColor;
}


- (CAAnimationGroup *)groupAniIndex:(int)index {
    CAAnimationGroup * group = [CAAnimationGroup animation];
    group.repeatCount = HUGE;
    group.autoreverses = NO;
    group.fillMode = kCAFillModeForwards;
    group.duration = self.diffusionDuration;
    CFTimeInterval time = CACurrentMediaTime() + (double)(index * self.diffusionDuration) / (double)self.diffusionCount;
    group.beginTime = time;
    group.removedOnCompletion = NO;

    return group;
}

- (CABasicAnimation *)animate_scale {
    CABasicAnimation * ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    ani.fromValue = @(1);
//    ani.toValue = @([UIScreen mainScreen].bounds.size.width/self.bounds.size.width);
    ani.toValue = @(self.diffusionScale);

    return ani;
}

- (CABasicAnimation *)animate_borderWidth {
    CABasicAnimation * ani = [CABasicAnimation animationWithKeyPath:@"borderWidth"];
    ani.fromValue = @(self.diffusionWidth);
    ani.toValue = @(self.bounds.size.width/[UIScreen mainScreen].bounds.size.width*self.diffusionWidth);
    

    return ani;
}

- (CABasicAnimation *)animate_alpha {
    CABasicAnimation * ani = [CABasicAnimation animationWithKeyPath:@"opacity"];
    ani.fromValue = @(0.8);
    ani.toValue = @(0.1);

    return ani;
}

- (CAKeyframeAnimation *)animate_backGroundColor {
    
    CAKeyframeAnimation *backgroundColorAnimation = [CAKeyframeAnimation animation];

    backgroundColorAnimation.keyPath = @"backgroundColor";
    backgroundColorAnimation.values = @[(__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:0.3].CGColor,
                                        (__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:0.2].CGColor,
                                        (__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:0.1].CGColor,
                                        (__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:0].CGColor];

    backgroundColorAnimation.keyTimes = @[@0.3,@0.6,@0.9,@1];
    
    
//    backgroundColorAnimation.values = @[(__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:1].CGColor,
//                                        (__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:0.7].CGColor,
//                                        (__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:0.4].CGColor,
//                                        (__bridge id)[[UIColor colorWithHexString:@"#B3C27C"] alphaColor:0.1].CGColor];
//
//    backgroundColorAnimation.keyTimes = @[@0.3,@0.6,@0.9,@1];
    
    return backgroundColorAnimation;
    
}



- (CALayer *)pulsingLayer:(CGRect)rect animation:(CAAnimationGroup *)animationGroup {
    
    CALayer *pulsingLayer = [CALayer layer];

    CGSize size = self.bounds.size;
    pulsingLayer.frame = CGRectMake((rect.size.width-size.width)/2, (rect.size.height-size.height)/2, size.width, size.height);
    pulsingLayer.borderColor = self.diffusionColor.CGColor;

    pulsingLayer.cornerRadius = size.height / 2;
    [pulsingLayer addAnimation:animationGroup forKey:@"plulsing"];

    return pulsingLayer;
}



@end
