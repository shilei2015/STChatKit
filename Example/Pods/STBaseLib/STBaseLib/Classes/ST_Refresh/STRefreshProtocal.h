//
//  STItem.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import <Foundation/Foundation.h>
#import "STRefreshConst.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    STItemType_TableView_Cell,
    STItemType_TableView_Header,
    STItemType_TableView_Footer,

    STItemType_CollectionView_Cell,
    STItemType_CollectionView_Header,
    STItemType_CollectionView_Footer,

} STItemType;


/// 列表页面数据遵守的协议
@protocol STItem <NSObject>

@optional
///配置ItemCell对应的Class 若列表设置了 itemCellClass  则不用实现
- (Class)itemCellClassWithItemType:(STItemType)itemType listViewType:(NSString *)listViewType;


///配置ItemCell 或 Header Footer高度
- (CGFloat)itemHeightWithItemType:(STItemType)itemType listViewType:(NSString *)listViewType;

@end










@protocol STCell <NSObject>

///ItemCell 设置数据  遵守协议的Cell重写set方法    添加 @synthesize itemCell_TempModel = _itemCell_TempModel;
@property (nonatomic,strong) id itemCell_TempModel;

@optional
+ (BOOL)isRegisterNib;
- (void)didEndDisplayForRowAtIndexPath:(NSIndexPath *)indexPath;

@end









@protocol STListRefreshNetWork <NSObject>

+ (void)refreshNetWithApiName:(NSString *)apiStr parms:(NSDictionary *)requestParms requestType:(RefreshNetWorkType)type successBlock:(RefreshNetComplectBlock)complectBlock failureBlock:(RefreshNetFailureBlock)failureBlock;

@end








NS_ASSUME_NONNULL_END
