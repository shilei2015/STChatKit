//
//  STNetworking.h
//  STBase
//
//  Created by 石磊 on 2022/11/7.
//

#import <Foundation/Foundation.h>
#import "STNetworkConst.h"
#import "YHNet.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^RequestHelperComplect) (NSDictionary * responseDict,NSInteger code,NSDictionary * _Nullable data,NSError * _Nullable error);

@protocol SDKNetworkingProtocal <NSObject>

///网络请求基础路径
+ (NSString *)baseUrlString_default;

+ (NSString *)baseUrlString_debug;

+ (NSString *)baseUrlString_release;

///网路请求
+ (void)request:(NSString *)apiStr parms:(nullable NSDictionary *)requestParms SuccessBlock:(nullable RequestHelperComplect)networkComplect loadingHud:(BOOL)showHud errorTip:(BOOL)tipError;


///上传图片视频
+ (void)uploadImages:(NSArray<UIImage *> * _Nullable)items loading:(BOOL)loading success:(nullable void(^)(NSArray <NSString *> *urlStrs))success fail:(nullable void(^)(NSError * error))fail;

+ (void)uploadFiles:(NSArray<YHUploadFileModel *> *)files
                 url:(NSString *)url
        successBlock:(nullable YHHttpUpImageSuccessBlock)successBlock
          errorBlock:(nullable YHHttpRequestErrorBlock)errorBlock
            showHud:(BOOL)showHud;

+ (void)cancelRequestWithURL:(nullable NSString *)url;

///设置公共header 和 参数
+ (void)signtureDealWithHeaderDict:(NSMutableDictionary *)header requestParms:(NSMutableDictionary *)requestParms;


///当网络请求需要提示信息时 调用
+ (NSString *)netWorkTipString:(NSDictionary *)responseDict;


@end








@interface STBaseNetworking : NSObject

+ (Class<SDKNetworkingProtocal>)appNetworkClass;

+ (NSString *)baseUrlString;

///网络请求配置
+ (void)configAppNetworkClass:(Class<SDKNetworkingProtocal>)networkClass;

//+ (void)configBaseUrlstring:(NSString *)baseUrlString;

+ (void)requestWithApiName:(NSString *)apiStr parms:(nullable NSDictionary *)requestParms requestType:(YHHttpMethod)type SuccessBlock:(nullable NetComplectBlock)complectBlock;

+ (void)requestWithApiName:(NSString *)apiStr parms:(nullable NSDictionary *)requestParms requestType:(YHHttpMethod)type SuccessBlock:(nullable NetComplectBlock)complectBlock loadingHud:(BOOL)showHud errorTip:(BOOL)tipError;



///取消某个请求
+ (void)cancelRequestWithURL:(nullable NSString *)url;

/// 取消所有的网络请求
+ (void)cancelAllRequest;



///文件上传
+ (nullable NSURLSessionDataTask *)uploadWithURL:(NSString *)url
                                           files:(NSArray<YHUploadFileModel *> *)files
                                           param:(nullable id)param
                                         headers:(nullable NSDictionary<NSString *, NSString *> *)headers
                                      isUseHttps:(BOOL)isUseHttps
                                   progressBlock:(nullable YHHttpRequestProgressBlock)progressBlock
                                    successBlock:(nullable YHHttpRequestSuccessBlock)successBlock
                                      errorBlock:(nullable YHHttpRequestErrorBlock)errorBlock showHud:(BOOL)showHud;



+ (void)uploadImages:(NSArray<UIImage *> *)images
             preDeal:(nullable void(^)(UIImage *image))preDeal
                 url:(NSString *)url
        successBlock:(nullable YHHttpUpImageSuccessBlock)successBlock
          errorBlock:(nullable YHHttpRequestErrorBlock)errorBlock
             showHud:(BOOL)showHud;


+ (void)uploadFiles:(NSArray<YHUploadFileModel *> *)files
                 url:(NSString *)url
        successBlock:(nullable YHHttpUpImageSuccessBlock)successBlock
          errorBlock:(nullable YHHttpRequestErrorBlock)errorBlock
            showHud:(BOOL)showHud;



+ (void)testSemaphore;

@end
NS_ASSUME_NONNULL_END
