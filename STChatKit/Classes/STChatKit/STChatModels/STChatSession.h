//
//  STChatSession.h
//  STChatKit
//
//  Created by EDY on 2023/7/12.
//

#import <Foundation/Foundation.h>

@class STChatMessage;

NS_ASSUME_NONNULL_BEGIN

@interface STChatSession : NSObject

///最新消息
@property (nonatomic,strong) STChatMessage *lastMessage;

///频道id
@property(nonatomic, copy) NSString *channelId;

///未读消息数
@property (nonatomic , assign) NSInteger unReadCount;


+ (NSString *)tableName;

///获取全部会话
+ (NSArray <STChatSession *>*)allSessions;

///全部未读消息数量
+ (NSInteger)allSessionUnreadCount;

///获取一条会话
+ (STChatSession *)sessionWithLastMsg:(STChatMessage *)message;

+ (STChatSession *)sessionWithPeer:(NSString *)peer;

///清空会话列表中的回话
+ (void)deleteAllSession;

///标记会话已读，清除当前会话全部未读
- (void)markAllRead;

///保存
- (void)saveToTable;

///删除当前会话
- (void)removeSession;

@end


NS_ASSUME_NONNULL_END
