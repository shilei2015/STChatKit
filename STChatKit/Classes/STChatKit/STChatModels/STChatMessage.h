//
//  STChatMessage.h
//  STChatKit
//
//  Created by EDY on 2023/7/12.
//

#import <Foundation/Foundation.h>
#import "STChatDefine.h"

@class STChatUser,STChatMessage,STChatSession,STChatImage;

NS_ASSUME_NONNULL_BEGIN

@interface STChatMessage : NSObject

+ (NSArray *)messagesWithChannel:(NSString *)channel;

+ (instancetype)messageWithText:(NSString *)text;

+ (instancetype)messageWithImage:(UIImage *)image;

+ (instancetype)messageWithGiftUrlString:(NSString *)urlString relationId:(NSString *)RelationId;


+ (NSArray *)getMessageWithCount:(NSInteger)count channelId:(NSString *)channelId firstMessage:(nullable STChatMessage *)firstMessage complect:(nullable void(^)(NSArray <STChatMessage *>*messages))complect;


///自定义消息的tag 用于区分发送的消息
@property(nonatomic, copy) NSString *actionTag;

///是否忽略同步消息。sychonizeIgnore == YES 忽略
@property (nonatomic , assign) BOOL sychonizeIgnore;

///发送者Id
@property(nonatomic, copy) NSString *fromUid;

///接收者Id
@property(nonatomic, copy) NSString *toUid;

///频道id   (fromUid 与 toUid 的组合) 小_大
@property(nonatomic, copy) NSString *channelId;

///消息类型
@property(nonatomic, assign) STChatMessageType msgType;

///文字类型消息内容
@property(nonatomic, copy) NSString *textMessage;

///图片消息体
@property (nonatomic,strong) STChatImage *imageObj;

///是否已读
@property (nonatomic , assign) BOOL isRead;

///是否本人发送
@property (nonatomic , assign) BOOL isMineSend;

///扩展
@property (nonatomic , strong) NSDictionary * ext;

///本地扩展
@property (nonatomic , strong) NSDictionary * localExt;

///本地id
@property(nonatomic, copy) NSString *messageId;

///消息发送状态
@property(nonatomic, assign) ChatSendComplectType complectType;

///创建时间
@property (nonatomic , assign) NSInteger createTimeSpan;

///更新时间
@property (nonatomic , assign) NSInteger updateTimeSpan;

///更新（发送状态）回调
@property (nonatomic , copy) LDMessageUpdateHandle updateHandle;

@property (nonatomic , assign) NSInteger noSaveType;

@property (nonatomic , assign) BOOL isAfter5Min;


@property (nonatomic , copy) NSString * group;

///清除频道内全部消息
+ (void)deleteRecordWithChannelId:(NSString *)channelId;

///聊天对象id
- (NSString *)chatTargetUid;

///发送者
- (STChatUser *)fromUser;

///接受者
- (STChatUser *)toUser;

///聊天对象
- (STChatUser *)chatTargetUser;

///保存当前消息
- (void)saveMsgToTable;

- (NSString *)contentText;

+ (NSString *)groupTableNameWithGroup:(NSString *)groupId;
+ (NSString *)tableNameNameWithPeer:(NSString *)peer;

@end

NS_ASSUME_NONNULL_END
