//
//  STBaseNav.m
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import "STBaseNav.h"

@interface STBaseNav ()

@end

@implementation STBaseNav


// 重写自定义的UINavigationController中的push方法
// 处理tabbar的显示隐藏
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
     if (self.childViewControllers.count >= 1) {
         viewController.hidesBottomBarWhenPushed = YES; //viewController是将要被push的控制器
     }
     [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    return [super popViewControllerAnimated:animated];
}


@end
