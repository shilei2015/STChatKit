//
//  STBaseWebVC.h
//  NoName
//
//  Created by 石磊 on 2023/2/27.
//


#import "STBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface STBaseWebVC : STBaseVC

///加载网页地址
+ (instancetype)webWithTitle:(nullable NSString *)title url:(NSString *)url;

///加载 HTMLString
+ (instancetype)webWithTitle:(nullable NSString *)title htmlString:(NSString *)htmlString;

@property(nonatomic, copy) NSString *bgColorHex;

@property(nonatomic, copy) NSString *textColorHex;

@property (nonatomic , assign) BOOL showLoadingInView;


@end

NS_ASSUME_NONNULL_END
