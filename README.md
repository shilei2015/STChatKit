# STChatKit

[![CI Status](https://img.shields.io/travis/shilei2015/STChatKit.svg?style=flat)](https://travis-ci.org/shilei2015/STChatKit)
[![Version](https://img.shields.io/cocoapods/v/STChatKit.svg?style=flat)](https://cocoapods.org/pods/STChatKit)
[![License](https://img.shields.io/cocoapods/l/STChatKit.svg?style=flat)](https://cocoapods.org/pods/STChatKit)
[![Platform](https://img.shields.io/cocoapods/p/STChatKit.svg?style=flat)](https://cocoapods.org/pods/STChatKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

STChatKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'STChatKit'
```

## Author

shilei2015, 244235126@qq.com

## License

STChatKit is available under the MIT license. See the LICENSE file for more info.
