//
//  UIImageView+SDImage.m
//  Asocial
//
//  Created by 石磊 on 2022/10/27.
//

#import "UIImageView+SDImage.h"
#import <SDWebImage/SDWebImage.h>
#import <Masonry/Masonry.h>
#import <objc/runtime.h>


@interface UIImageView (SDImage)

@property (nonatomic,strong) UIActivityIndicatorView *indView;

@end

@implementation UIImageView (SDImage)

- (void)setIndView:(UIActivityIndicatorView *)indView {
    objc_setAssociatedObject(self, @selector(indView), indView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIActivityIndicatorView *)indView {
    return objc_getAssociatedObject(self, @selector(indView));
}

- (void)setUrlString:(NSString *)urlString {
    objc_setAssociatedObject(self, @selector(urlString), urlString, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self sd_setUrlString:urlString];
}

- (NSString *)urlString {
    return objc_getAssociatedObject(self, @selector(urlString));
}


/*----------------------分割线------------------------*/
- (void)sd_setUrlString:(NSString *)urlString {
    [self sd_setUrlString:urlString loadingType:(STActivityIndicatorViewStyleNone)];
}

- (void)sd_loadingWithUrlStirng:(NSString *)urlString {
    [self sd_setUrlString:urlString loadingType:(STActivityIndicatorViewStyleGray)];
}

- (void)sd_setUrlString:(NSString *)urlString loadingType:(STActivityIndicatorViewStyle)style {
        
    if (!self.indView) {
        UIActivityIndicatorView * tempIndView;
        if (style == STActivityIndicatorViewStyleGray) {
            tempIndView = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        } else if (style == STActivityIndicatorViewStyleWhite) {
            tempIndView = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        } else if (style == STActivityIndicatorViewStyleWhiteLarge) {
            tempIndView = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        
        self.indView = tempIndView;
        
        [self addSubview:self.indView];
        __weak typeof(self) bself = self;
        [self.indView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(bself);
        }];
    }
    
    [self sd_cancelCurrentImageLoad];
    self.indView.hidden = NO;
    [self.indView startAnimating];
    
    __weak typeof(self) bself = self;
    [self sd_setImageWithURL:[NSURL URLWithString:urlString] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                bself.indView.hidden = YES;
            });
        }
    }];
}


+ (void)cacheImage:(UIImage *)image urlstring:(NSString *)urlString {
    [[SDImageCache sharedImageCache] storeImage:image forKey:urlString completion:nil];
}

@end
