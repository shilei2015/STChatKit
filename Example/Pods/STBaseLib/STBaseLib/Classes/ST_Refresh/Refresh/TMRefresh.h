//
//  TMRefresh.h
//  TMM
//
//  Created by  jun on 2019/11/10.
//  Copyright © 2019 yinhe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJRefresh/MJRefresh.h>

NS_ASSUME_NONNULL_BEGIN

@interface TMRefreshHeader : MJRefreshNormalHeader

@end

@interface TMRefreshFooter : MJRefreshBackNormalFooter

@end

NS_ASSUME_NONNULL_END
