//
//  STCHATViewController.m
//  STChatKit
//
//  Created by shilei2015 on 07/12/2023.
//  Copyright (c) 2023 shilei2015. All rights reserved.
//

#import "STCHATViewController.h"
#import <STChatKit/STChatKit.h>
#import <STBaseLib/STBaseLib.h>

static NSString * const kPeer = @"2";

@interface STCHATViewController ()<STListRefreshNetWork>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation STCHATViewController

+ (void)load {
    [STBaseNetworking configAppNetworkClass:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
//    [STChatManager configChatUid:@"122"];
//
//    self.tableView.cellConfig = ^(UITableViewCell *  _Nonnull cell, STChatMessage *  _Nullable msg) {
//        cell.textLabel.text = [NSString stringWithFormat:@"%@",msg.textMessage];
//        cell.textLabel.numberOfLines = 0;
//    };
//
//    {
//        self.tableView.backgroundColor = [UIColor redColor];
//
//        [STChatManager configChatTableView:self.tableView pageSize:10 firstLoad:YES targetUid:kPeer itemCellClass:^Class _Nullable(id  _Nullable msg) {
//            return UITableViewCell.class;
//        } getNewMessage:^(STChatMessage * _Nonnull message, STChatSession * _Nonnull session) {
//
//        }];
//    }
    
}

- (IBAction)addNewMessage:(id)sender {
    static int i = 0;
    i ++;
    STChatMessage * message = [STChatMessage messageWithText:[NSString stringWithFormat:@"Text - %d",i]];

    [STChatManager sendMessage:message toUser:kPeer complect:^(STChatMessage * _Nullable message, id  _Nullable data) {
        [STChatManager tableView:self.tableView addMessage:message];
    }];
    
    message.updateHandle = ^(STChatMessage * _Nullable message) {
        
    };
}


- (IBAction)reloadList:(id)sender {
    [self.tableView.singleListItems removeAllObjects];
    [self.tableView startHeadRefreshWithAnimation:YES];
}

///获取消息列表
- (IBAction)getMessageList:(UIButton *)sender {
    NSLog(@"获取列表【ChatMessageTable_2_122】");
    [[STChatMessage bg_findAll:@"ChatMessageTable_2_122"] enumerateObjectsUsingBlock:^(STChatMessage * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSLog(@"%@",obj.textMessage);
    }];
}

- (IBAction)clearMsgs:(id)sender {
    
    [STChatMessage deleteRecordWithChannelId:[STChatManager channelWithUid:kPeer]];
    
    [self.tableView.singleListItems removeAllObjects];
    [self.tableView startHeadRefreshWithAnimation:YES];
}


@end
