//
//  MBProgressHUD+Hud.m
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import "MBProgressHUD+Hud.h"


@implementation MBProgressHUD (Hud)

//MARK: Normal Tip
/**
 *  =======显示 提示信息
 *  @param tip 信息内容
 */
+ (void)showTip:(NSString *)tip
{
    [self showTip:tip toView:nil];
}

/**
 *  =======显示
 *  @param tip 信息内容
 */
+ (void)showTip:(NSString *)tip toView:(nullable UIView *)view
{
    [self show:tip icon:nil view:view];
}


//MARK: Success tip
/**
 *  =======显示 提示信息
 *  @param success 信息内容
 */
+ (void)showSuccess:(NSString *)success
{
    [self showSuccess:success toView:nil];
}

/**
 *  =======显示
 *  @param success 信息内容
 */
+ (void)showSuccess:(NSString *)success toView:(nullable UIView *)view
{
    [self show:success icon:nil view:view];
}


//MARK: Error tip
/**
 *  =======显示错误信息
 */
+ (void)showError:(NSString *)error
{
    [self showError:error toView:nil];
}

+ (void)showError:(NSString *)error toView:(UIView *)view{
    [self show:error icon:nil view:view];
}


//MARK: 菊花loading
/**
 *  显示提示 + 菊花
 *  @param message 信息内容
 */
+ (void)showMessage:(NSString *)message {
    [self showMessage:message toView:nil];
}


//MARK: 关闭Hud
/**
 *  手动关闭MBProgressHUD
 */
+ (void)hideHUD {
    [self hideHUDForView:nil];
}
/**
 *  @param view    显示MBProgressHUD的视图
 */
+ (void)hideHUDForView:(UIView *)view {
    
    //    __weak typeof(view) bview = view;
    __block UIView * bview = view;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (bview == nil) {
            bview = [MBProgressHUD keyWindow];
        }
        [self hideHUDForView:bview animated:YES];
    });
}









/**
 *  显示一些信息
 *  @param message 信息内容
 *  @param view    需要显示信息的视图
 */
+ (void)showMessage:(NSString *)message toView:(UIView *)view {
    [MBProgressHUD hideHUD];
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD bridgeShowMessage:message toView:view];
    });
}

+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view {
    [MBProgressHUD hideHUD];
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD bridgeShow:text icon:icon view:view];
    });
}




/**
 *  显示一些信息
 *  @param message 信息内容
 *  @param view    需要显示信息的视图
 *  @return 直接返回一个MBProgressHUD，需要手动关闭
 */
+ (MBProgressHUD *)bridgeShowMessage:(NSString *)message toView:(UIView *)view {
    if (view == nil) view = [MBProgressHUD keyWindow];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    if (message) {
        hud.label.text = message;
    } else {
        hud.label.text = @"";
    }
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    return hud;
}




/**
 *  =======显示信息
 *  @param text 信息内容
 *  @param icon 图标
 *  @param view 显示的视图
 */
+ (void)bridgeShow:(NSString *)text icon:(NSString *)icon view:(UIView *)view
{
    
    if (view == nil)
        view = [MBProgressHUD keyWindow];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.label.text = text;
    hud.label.numberOfLines = 3;
    
    hud.label.textColor = [UIColor whiteColor];
    hud.label.font = [UIFont systemFontOfSize:17.0];
    hud.userInteractionEnabled= NO;
    
    if (icon && icon.length) {
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:icon]];  // 设置图片
    }
    
    // 再设置模式
    hud.mode = MBProgressHUDModeText;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 1秒之后再消失
    [hud hideAnimated:YES afterDelay:1.5];
}



+ (UIWindow *)keyWindow {
    return [UIApplication sharedApplication].windows.firstObject;
}



@end
