//
//  STRegular.m
//  STBaseLib
//
//  Created by EDY on 2023/3/22.
//

#import "STRegular.h"

@implementation STRegular

+ (NSArray *)st_regular_match:(NSString *)regFormat from:(NSString *)fullString {
    
    NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:regFormat options:0 error:nil];
    NSArray * results = [regex matchesInString:fullString options:NSMatchingWithoutAnchoringBounds range:NSMakeRange(0, fullString.length)];
    
    return results;
}

@end
