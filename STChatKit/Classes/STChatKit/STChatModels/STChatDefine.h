//
//  STChatDefine.h
//  Pods
//
//  Created by EDY on 2023/7/12.
//

#ifndef STChatDefine_h
#define STChatDefine_h

@class STChatMessage;

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    STChatMessageTypeNone,
    STChatMessageTypeText = 1,
    STChatMessageTypeImage = 2,
    STChatMessageTypeGift = 5,
} STChatMessageType;

typedef enum {
    ChatSendComplectTypeOK,
    ChatSendComplectTypeSending,
    ChatSendComplectTypeFail,
} ChatSendComplectType;

typedef void(^LDMessageUpdateHandle)(STChatMessage * _Nullable message);


static NSString * const kUnReadCountKey = @"unReadCount";

NS_ASSUME_NONNULL_END


#endif /* STChatDefine_h */
