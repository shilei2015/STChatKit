//
//  NSArray+STBase.h
//  LDSDK
//
//  Created by 石磊 on 2023/2/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (STBase)

@end


@interface NSMutableArray (STBase)

- (void)sl_addObject:(id)anObject;

- (void)sl_addSkipNolengString:(NSString *)string;

@end




NS_ASSUME_NONNULL_END
