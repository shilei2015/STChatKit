//
//  UIDatePicker+STBase.h
//  LDSDK
//
//  Created by 石磊 on 2023/2/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDatePicker (STBase)

- (void)configPickerTextColor:(UIColor *)textColor;

@end

NS_ASSUME_NONNULL_END
