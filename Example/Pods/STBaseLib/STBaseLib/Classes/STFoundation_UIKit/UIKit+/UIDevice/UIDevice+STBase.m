//
//  UIDevice+STBase.m
//  LDSDK
//
//  Created by 石磊 on 2023/2/9.
//

#import "UIDevice+STBase.h"
#import "STIDKeyChain.h"
#import "STDebug.h"

@implementation UIDevice (STBase)

+ (CGFloat)width {
    return [UIScreen mainScreen].bounds.size.width;
}

+ (CGFloat)height {
    return [UIScreen mainScreen].bounds.size.height;
}

+ (NSString *)deviceId {
    if ([STDebug debugUserType] == 1) {
        return [STIDKeyChain randomIDFOrkey:@"user1"];
    } else if ([STDebug debugUserType] == 2) {
        return [STIDKeyChain randomIDFOrkey:@"user2"];
    }
    return [STIDKeyChain getDeviceIDInKeychain];
}

+ (void)changeDeviceId {
    [STIDKeyChain TestdeleteServer];
}



// open app settings.
+ (void)jumpToAppSettings {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
    });
}

+ (void)jumpToAppStoreWithAppID:(NSString *)appID complect:(void (^ __nullable)(BOOL success))complect {
    
    
    NSString *appURL = [NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@?action=write-review", appID];
    NSLog(appURL);
    
    if (!appID) { return; }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appURL] options:@{} completionHandler:complect];
        
    });
}


+ (void)jumpToAppStoreEVAWithAppID:(NSString *)appID complect:(void (^ __nullable)(BOOL success))complect {
    NSString *appURL = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@?action=write-review", appID];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appURL] options:@{} completionHandler:complect];
    });
}

+ (void)jumpToOutUrlstring:(NSString *)urlString {
    NSURL * url = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}


+ (void)languageAndCountryList {
    NSLocale *locale = [NSLocale currentLocale];
    {
        NSArray *countryArray = [NSLocale ISOCountryCodes];
        for (NSString *countryCode in countryArray) {
            NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
            NSString * lgnCode = [locale languageCode];
            NSLog(@"%@ , %@",displayNameString,countryCode);
        }
    }
    
    
    {
        NSArray *lgnArray = [NSLocale ISOLanguageCodes];
        for (NSString *lgnCode in lgnArray) {
            NSString *displayNameString = [locale displayNameForKey:NSLocaleLanguageCode value:lgnCode];
            NSLog(@"%@ , %@",displayNameString,lgnCode);
        }
    }
    
}


@end
