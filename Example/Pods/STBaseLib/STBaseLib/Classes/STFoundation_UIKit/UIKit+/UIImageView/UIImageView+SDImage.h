//
//  UIImageView+SDImage.h
//  Asocial
//
//  Created by 石磊 on 2022/10/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger, STActivityIndicatorViewStyle) {
    STActivityIndicatorViewStyleNone,
    STActivityIndicatorViewStyleWhiteLarge,
    STActivityIndicatorViewStyleWhite,
    STActivityIndicatorViewStyleGray,
};



@interface UIImageView (SDImage)


@property (nonatomic , copy)  NSString * _Nullable  urlString;

/// 加载图片 不显示loading
/// - Parameter urlString: 图片地址string
- (void)sd_setUrlString:(NSString *)urlString;


/// 加载图片 显示默认loading  STActivityIndicatorViewStyleGray
- (void)sd_loadingWithUrlStirng:(NSString *)urlString;


/// /// 加载图片 设置loading样式
/// - Parameters:
///   - urlString: 图片地址string
///   - style: loading显示样式
- (void)sd_setUrlString:(NSString *)urlString loadingType:(STActivityIndicatorViewStyle)style;

+ (void)cacheImage:(UIImage *)image urlstring:(NSString *)urlString;

@end

NS_ASSUME_NONNULL_END
