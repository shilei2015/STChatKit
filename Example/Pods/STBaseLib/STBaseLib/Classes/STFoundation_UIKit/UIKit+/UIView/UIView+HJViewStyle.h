//  IBInspectable
//  UIView+HJViewStyle.h
//  HJViewStyle
//
//  Created by JohnnyHoo on 2018/12/19.
//

#import <UIKit/UIKit.h>
@interface UIView (HJViewStyle)

typedef NS_ENUM(NSInteger, GradientStyle) {
    GradientStyleLeftToRight = 1,//渐变左到右
    GradientStyleTopToBottom = 2//渐变上到下
};

/*----------------------分割线------------------------*/
///头部圆角 top-right
@property (nonatomic, assign)  BOOL topRightRound;
///左边圆角 top-left
@property (nonatomic, assign)  BOOL topLeftRound;
///底部圆角 bottom-left
@property (nonatomic, assign)  BOOL bottomLeftRound;
///右边圆角 bottom-right
@property (nonatomic, assign)  BOOL bottomRightRound;

///圆
@property (nonatomic, assign)  BOOL circle;
///圆角
@property (nonatomic, assign)  CGFloat cornerRadius;

// 边圆角Layer
@property(nonatomic, strong) CAShapeLayer *maskLayer;


/*----------------------分割线------------------------*/
///边框宽度
@property (nonatomic, assign)  CGFloat borderWidth;
///边框颜色
@property (nonatomic, strong)  UIColor *borderColor;


/*----------------------分割线------------------------*/
///阴影颜色
@property (nonatomic, strong)  UIColor *shadowColor;
///阴影半径 默认1
@property (nonatomic, assign)  CGFloat shadowRadius;
///阴影透明度 默认1
@property (nonatomic, assign)  CGFloat shadowOpacity;
///阴影偏移
@property (nonatomic, assign)  CGSize shadowOffset;



/*----------------------分割线------------------------*/
///是否开启主题渐变风格 (很多APP渐变风格大多数都是一致了.为了更快设置颜色而添加的属性,具体用法实现UIView+Theme方法)
@property (nonatomic, assign)  BOOL themeGradientEnable;

///渐变方向
@property(nonatomic, assign) GradientStyle gradientStyle;
///渐变方向 xib用
@property(nonatomic, assign)  NSInteger gradientStyleEnum;
///渐变A颜色
@property (nonatomic, strong)  UIColor *gradientAColor;
///渐变B颜色
@property (nonatomic, strong)  UIColor *gradientBColor;

// 渐变Layer
@property(nonatomic, strong) CAGradientLayer *gradientLayer;

/*----------------------分割线------------------------*/
/*----------------------分割线------------------------*/
/*----------------------分割线------------------------*/

/// 阴影Layer
@property(nonatomic, strong) UIView *shadowView;




///上一次大小
@property (nonatomic, copy) NSString *lastSize;


- (void)refreshLayout;


@end





































@interface TempButton : UIButton

#if TARGET_INTERFACE_BUILDER
//MARK: - - - - - - - - - - - 圆角
///头部圆角 top-right
@property (nonatomic, assign) IBInspectable BOOL topRightRound;
///左边圆角 top-left
@property (nonatomic, assign) IBInspectable BOOL topLeftRound;
///底部圆角 bottom-left
@property (nonatomic, assign) IBInspectable BOOL bottomLeftRound;
///右边圆角 bottom-right
@property (nonatomic, assign) IBInspectable BOOL bottomRightRound;
///圆角
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
///圆 优先级高于上面的属性
@property (nonatomic, assign) IBInspectable BOOL circle;


//MARK: - - - - - - - - - - - 边框
 ///边框宽度
 @property (nonatomic, assign) IBInspectable CGFloat borderWidth;
 ///边框颜色
 @property (nonatomic, strong) IBInspectable UIColor *borderColor;



//MARK: - - - - - - - - - - - 阴影
 ///阴影颜色
 @property (nonatomic, strong) IBInspectable UIColor *shadowColor;
 ///阴影半径 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowRadius;
 ///阴影透明度 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowOpacity;
 ///阴影偏移
 @property (nonatomic, assign) IBInspectable CGSize shadowOffset;


#pragma mark 渐变
@property (nonatomic, assign) IBInspectable BOOL themeGradientEnable;
///渐变方向 xib用
@property(nonatomic, assign) IBInspectable NSInteger gradientStyleEnum;
///渐变A颜色
@property (nonatomic, strong) IBInspectable UIColor *gradientAColor;
///渐变B颜色
@property (nonatomic, strong) IBInspectable UIColor *gradientBColor;

#endif

@end

@interface TempLabel : UILabel

#if TARGET_INTERFACE_BUILDER
//MARK: - - - - - - - - - - - 圆角
///头部圆角 top-right
@property (nonatomic, assign) IBInspectable BOOL topRightRound;
///左边圆角 top-left
@property (nonatomic, assign) IBInspectable BOOL topLeftRound;
///底部圆角 bottom-left
@property (nonatomic, assign) IBInspectable BOOL bottomLeftRound;
///右边圆角 bottom-right
@property (nonatomic, assign) IBInspectable BOOL bottomRightRound;
///圆角
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
///圆 优先级高于上面的属性
@property (nonatomic, assign) IBInspectable BOOL circle;


//MARK: - - - - - - - - - - - 边框
 ///边框宽度
 @property (nonatomic, assign) IBInspectable CGFloat borderWidth;
 ///边框颜色
 @property (nonatomic, strong) IBInspectable UIColor *borderColor;



//MARK: - - - - - - - - - - - 阴影
 ///阴影颜色
 @property (nonatomic, strong) IBInspectable UIColor *shadowColor;
 ///阴影半径 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowRadius;
 ///阴影透明度 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowOpacity;
 ///阴影偏移
 @property (nonatomic, assign) IBInspectable CGSize shadowOffset;


#pragma mark 渐变
@property (nonatomic, assign) IBInspectable BOOL themeGradientEnable;
///渐变方向 xib用
@property(nonatomic, assign) IBInspectable NSInteger gradientStyleEnum;
///渐变A颜色
@property (nonatomic, strong) IBInspectable UIColor *gradientAColor;
///渐变B颜色
@property (nonatomic, strong) IBInspectable UIColor *gradientBColor;

#endif

@end

@interface TempImageView : UIImageView

#if TARGET_INTERFACE_BUILDER
//MARK: - - - - - - - - - - - 圆角
///头部圆角 top-right
@property (nonatomic, assign) IBInspectable BOOL topRightRound;
///左边圆角 top-left
@property (nonatomic, assign) IBInspectable BOOL topLeftRound;
///底部圆角 bottom-left
@property (nonatomic, assign) IBInspectable BOOL bottomLeftRound;
///右边圆角 bottom-right
@property (nonatomic, assign) IBInspectable BOOL bottomRightRound;
///圆角
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
///圆 优先级高于上面的属性
@property (nonatomic, assign) IBInspectable BOOL circle;


//MARK: - - - - - - - - - - - 边框
 ///边框宽度
 @property (nonatomic, assign) IBInspectable CGFloat borderWidth;
 ///边框颜色
 @property (nonatomic, strong) IBInspectable UIColor *borderColor;



//MARK: - - - - - - - - - - - 阴影
 ///阴影颜色
 @property (nonatomic, strong) IBInspectable UIColor *shadowColor;
 ///阴影半径 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowRadius;
 ///阴影透明度 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowOpacity;
 ///阴影偏移
 @property (nonatomic, assign) IBInspectable CGSize shadowOffset;


#pragma mark 渐变
@property (nonatomic, assign) IBInspectable BOOL themeGradientEnable;
///渐变方向 xib用
@property(nonatomic, assign) IBInspectable NSInteger gradientStyleEnum;
///渐变A颜色
@property (nonatomic, strong) IBInspectable UIColor *gradientAColor;
///渐变B颜色
@property (nonatomic, strong) IBInspectable UIColor *gradientBColor;

#endif

@end




@interface TempDesign_r : UIView

#if TARGET_INTERFACE_BUILDER

//MARK: - - - - - - - - - - - 圆角
 ///头部圆角 top-right
 @property (nonatomic, assign) IBInspectable BOOL topRightRound;
 ///左边圆角 top-left
 @property (nonatomic, assign) IBInspectable BOOL topLeftRound;
 ///底部圆角 bottom-left
 @property (nonatomic, assign) IBInspectable BOOL bottomLeftRound;
 ///右边圆角 bottom-right
 @property (nonatomic, assign) IBInspectable BOOL bottomRightRound;

///圆角
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

 ///圆 优先级高于上面的属性
 @property (nonatomic, assign) IBInspectable BOOL circle;

#endif

@end



@interface TempDesign_b : UIView

#if TARGET_INTERFACE_BUILDER
//MARK: - - - - - - - - - - - 边框
 ///边框宽度
 @property (nonatomic, assign) IBInspectable CGFloat borderWidth;
 ///边框颜色
 @property (nonatomic, strong) IBInspectable UIColor *borderColor;

#endif

@end


@interface TempDesign_rb : UIView

#if TARGET_INTERFACE_BUILDER

//MARK: - - - - - - - - - - - 圆角
 ///头部圆角 top-right
 @property (nonatomic, assign) IBInspectable BOOL topRightRound;
 ///左边圆角 top-left
 @property (nonatomic, assign) IBInspectable BOOL topLeftRound;
 ///底部圆角 bottom-left
 @property (nonatomic, assign) IBInspectable BOOL bottomLeftRound;
 ///右边圆角 bottom-right
 @property (nonatomic, assign) IBInspectable BOOL bottomRightRound;

///圆角
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

 ///圆 优先级高于上面的属性
 @property (nonatomic, assign) IBInspectable BOOL circle;

//MARK: - - - - - - - - - - - 边框
 ///边框宽度
 @property (nonatomic, assign) IBInspectable CGFloat borderWidth;
 ///边框颜色
 @property (nonatomic, strong) IBInspectable UIColor *borderColor;

#endif

@end




@interface TempDesign_s : UIView

#if TARGET_INTERFACE_BUILDER
//MARK: - - - - - - - - - - - 阴影
 ///阴影颜色
 @property (nonatomic, strong) IBInspectable UIColor *shadowColor;
 ///阴影半径 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowRadius;
 ///阴影透明度 默认1
 @property (nonatomic, assign) IBInspectable CGFloat shadowOpacity;
 ///阴影偏移
 @property (nonatomic, assign) IBInspectable CGSize shadowOffset;

#endif

@end


@interface TempDesign_g : UIView

#if TARGET_INTERFACE_BUILDER
//MARK: - - - - - - - - - - - 渐变
 ///是否开启主题渐变风格 (很多APP渐变风格大多数都是一致了.为了更快设置颜色而添加的属性,具体用法实现UIView+Theme方法)
 @property (nonatomic, assign) IBInspectable BOOL themeGradientEnable;
 ///渐变方向 xib用
 @property(nonatomic, assign) IBInspectable NSInteger gradientStyleEnum;
 ///渐变A颜色
 @property (nonatomic, strong) IBInspectable UIColor *gradientAColor;
 ///渐变B颜色
 @property (nonatomic, strong) IBInspectable UIColor *gradientBColor;
#endif

@end

