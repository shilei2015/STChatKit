//
//  STTimer.m
//  LDSDK
//
//  Created by 石磊 on 2023/2/3.
//

#import "ST_Timer.h"
#import <LSTTimer/LSTTimer.h>
#import "STDebug.h"

@implementation ST_Timer

+ (void)startTimerFor:(NSString *)identifer handle:(void(^)(void))handle {
    
    [LSTTimer removeTimerForIdentifier:identifer]; 
    [LSTTimer addMinuteTimerForTime:60*60*6 identifier:identifer handle:^(NSString * _Nonnull day, NSString * _Nonnull hour, NSString * _Nonnull minute, NSString * _Nonnull second, NSString * _Nonnull ms) {
        
        if ([ST_Timer enableLog]) {
            NSLog(@"【identifer = %@】%@天 %@时 %@分 %@秒 %@毫秒",identifer,day,hour,minute,second,ms);
        }
        if (handle) {
            handle();
        }
    } finish:^(NSString * _Nonnull identifier) {
        if ([ST_Timer enableLog]) {
            LDLog(@"timer finish 【%@】",identifier);
        }
    } pause:^(NSString * _Nonnull identifier) {
        if ([ST_Timer enableLog]) {
            LDLog(@"timer pause【%@】",identifier);
        }
    }];
    
}

+ (void)removeTimerWithIdentifer:(NSString *)identifer {
    [LSTTimer removeTimerForIdentifier:identifer];
}

+ (STTime)timeWithTimeInterval:(NSInteger)timeInterval {
    
    STTime time;
    time.total = timeInterval;
    time.day = timeInterval/60/60/24;
    time.hour =  timeInterval/60/60%24;
    time.minute = (timeInterval/60)%60;
    time.second = timeInterval%60;
    
    
    return time;
}

+ (BOOL)enableLog {
    return [STDebug enableLog_timer];
}

@end



