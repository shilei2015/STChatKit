
#import "STPoper.h"
#import "NSObject+STBase.h"
#import "STBlurView.h"
#import "STDebug.h"
#import <Masonry/Masonry.h>

static NSString * const kPopDismissHandleKey = @"STPopDismiss";

@interface STPoper ()

@property (nonatomic,strong) LSTPopView *popView;
//@property (nonatomic,weak) LSTPopView *popView;

@property (nonatomic,weak) UIView *showView;

@property (nonatomic , assign) NSInteger tst;

@end




@implementation STPoper


+ (void)load {
    [LSTPopView setLogStyle:LSTPopViewLogStyleConsole];
}

+ (instancetype)st_popShowView:(UIView *)showView position:(LDPopPosition)position blur:(BOOL)blur bgDismiss:(BOOL)bgDismiss {
    return [self st_popShowView:showView parentView:nil position:position blur:blur bgDismiss:bgDismiss config:nil];
}

+ (instancetype)st_popShowView:(UIView *)showView parentView:(nullable UIView *)pView position:(LDPopPosition)position blur:(BOOL)blur bgDismiss:(BOOL)bgDismiss {
    return [self st_popShowView:showView parentView:pView position:position blur:blur bgDismiss:bgDismiss config:nil];
}

+ (instancetype)st_popShowView:(UIView *)showView position:(LDPopPosition)position blur:(BOOL)blur  bgDismiss:(BOOL)bgDismiss config:(nullable void(^)(LSTPopView *popView))configHandle {
    return [self st_popShowView:showView parentView:nil position:position blur:blur bgDismiss:bgDismiss config:configHandle];
}


+ (instancetype)st_popShowView:(UIView *)showView parentView:(nullable UIView *)pview position:(LDPopPosition)position blur:(BOOL)blur bgDismiss:(BOOL)bgDismiss config:(nullable void(^)(LSTPopView *popView))configHandle {
    STPoper * tempHelper = [self new];
    {
        
        LSTPopView *popView;
        if (pview) {
            popView = [LSTPopView initWithCustomView:showView parentView:pview popStyle:LSTPopStyleFade dismissStyle:LSTDismissStyleFade];
        } else {
            popView = [LSTPopView initWithCustomView:showView];
        }
        
        popView.popDuration = 0.35;
        popView.dismissDuration = 0.35;

        popView.dragStyle = LSTDragStyleNO;
        popView.sweepStyle = LSTSweepStyleNO;
        popView.sweepDismissStyle = LSTSweepDismissStyleVelocity;
        popView.isClickBgDismiss = bgDismiss;
        popView.isStack = YES;
        
        
        if (position == LDPopPositionTop) {
            
            popView.popStyle = LSTPopStyleSmoothFromTop;
            popView.dismissStyle = LSTDismissStyleSmoothToTop;
            popView.hemStyle = LSTHemStyleTop;
            
        } else if (position == LDPopPositionCenter) {
            
            popView.popStyle = LSTPopStyleFade;
            popView.dismissStyle = LSTDismissStyleFade;
            popView.hemStyle = LSTHemStyleCenter;
            
        } else if (position == LDPopPositionBottom) {
            
            popView.hemStyle = LSTHemStyleBottom;
            popView.popStyle = LSTPopStyleSmoothFromBottom;
            popView.dismissStyle = LSTDismissStyleSmoothToBottom;
            
        }
        
        
        if (blur) {
            [self blurMaskForPop:popView];
        }
        
        //是否需要配置
        if (configHandle) {configHandle(popView);}
        
        tempHelper.popView = popView;
        
        __weak typeof(tempHelper) btempHelper = tempHelper;
        
        [showView addHandle:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
            [btempHelper dismiss];
        } withIdentifer:kPopDismissHandleKey];
        
    }
    tempHelper.showView = showView;
    
    return tempHelper;
}

+ (void)dismissView:(UIView *)view {

    dispatch_async(dispatch_get_main_queue(), ^{
       
    if ([view.superview.superview isKindOfClass:[LSTPopView class]]) {
        LSTPopView * popView = (LSTPopView *)view.superview.superview;
        [popView dismiss];
    } else {
        if ([view handleWithIdentifier:kPopDismissHandleKey]) {
            [view handleWithIdentifier:kPopDismissHandleKey](view,0,nil,@"dismiss");
        }
    }
       
    });
    
    
}

+ (void)blurMaskForPop:(LSTPopView *)popView {
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    UIView * popBG = [popView valueForKey:@"backgroundView"];
    UIBlurEffect * effect = [UIBlurEffect effectWithStyle:(UIBlurEffectStyleDark)] ;
    STBlurView * blurView = [[STBlurView alloc] initWithEffect:effect];
    blurView.customIntensity = 0.4;
    blurView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
    [popBG addSubview:blurView];
    [popBG sendSubviewToBack:blurView];
    blurView.alpha = 0;
    [UIView animateWithDuration:0.35 animations:^{
        blurView.alpha = 1;
    }];
    
    
}

- (void)show {
    [self.popView pop];
}

- (void)dismiss {
    [self.popView dismiss];
    self.popView = nil;
    self.showView = nil;
}


- (void)dealloc {
    [self subDealloc];
    NSString * logT = [NSString stringWithFormat:@"__dealloc__❤️❤️❤️❤️❤️❤️❤️❤️ %@",NSStringFromClass([self class])];
    LDLog(@"%@",logT);
}

- (void)subDealloc {}


@end
