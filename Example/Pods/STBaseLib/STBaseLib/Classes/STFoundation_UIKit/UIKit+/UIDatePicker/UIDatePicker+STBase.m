//
//  UIDatePicker+STBase.m
//  LDSDK
//
//  Created by 石磊 on 2023/2/9.
//

#import "UIDatePicker+STBase.h"

@implementation UIDatePicker (STBase)


- (void)configPickerTextColor:(UIColor *)textColor {
    
    if ([self respondsToSelector:@selector(setValue:forKey:)]) {
        [self setValue:textColor forKey:@"textColor"];
    }
    SEL selector = NSSelectorFromString(@"setHighlightsToday:");
    //创建NSInvocation
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:self];
}

@end
